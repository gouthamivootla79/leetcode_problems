package org.dp.practice.DP_ON_1D;

/*
leetcode : https://leetcode.com/problems/min-cost-climbing-stairs/description/
You are given an integer array cost where cost[i] is the cost of ith step on a staircase. Once you pay the cost, you can either climb one or two steps.
You can either start from the step with index 0, or the step with index 1.

Input: cost = [10,15,20]
Output: 15
Explanation: You will start at index 1.
- Pay 15 and climb two steps to reach the top.
The total cost is 15.
 */
public class MinCostClimbingStairs  {
    public static void main(String[] args) {
        int[] cost = {10,15,20};
        int n = cost.length;
        int[] dp = new int[n+1];

        System.out.println(minCost(cost, n, dp));
    }

    private static int minCost(int[] cost, int n, int[] dp) {
        dp[0] = cost[0];
        dp[1] = cost[1];

        for(int i=2;i<n;i++){
            dp[i] = cost[i] + Math.min(dp[i-1], dp[i-2]);
        }

        return Math.min(dp[n-1],dp[n-2]);
    }
}

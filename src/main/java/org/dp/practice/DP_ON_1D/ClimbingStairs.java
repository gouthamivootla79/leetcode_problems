package org.dp.practice.DP_ON_1D;

//https://leetcode.com/problems/climbing-stairs/description/
//This solution is similar to fibonacci question f(n) = f(n-1) + f(n-2)
public class ClimbingStairs {
    public static void main(String[] args) {
        int[] steps = {1, 2};
        int n = 3;
        int[][] dp = new int[steps.length + 1][n + 1];
        int[] newDp = new int[n + 1];

        System.out.println(minSteps(dp, n, steps));
        System.out.println(distinctSteps(dp, n, steps));
        System.out.println(climbStairs(newDp, 3));
    }

    //Correct solution to the above question. It find different distinct ways to reach top of stairs.
    private static int climbStairs(int[] dp, int n) {
        dp[0] = 0;
        if (n >= 1) {
            dp[1] = 1;
            if (n >= 2) {
                dp[2] = 2;

                for (int i = 3; i <= n; i++) {
                    dp[i] = dp[i - 1] + dp[i - 2];
                }
            }
        }
        return dp[n];
    }

    //This is true to count the distinct steps, but let say 1->2 and 2->1 , these are treated as 1 distinct way . But these are 2 different ways to reach top.
    private static int distinctSteps(int[][] dp, int n, int[] steps) {

        for (int i = 0; i <= steps.length; i++) {
            for (int j = 0; j <= n; j++) {
                if (i == 0 && j == 0) {
                    dp[i][j] = 1;
                } else if (j == 0) {
                    dp[i][j] = 1;
                } else if (i == 0) {
                    dp[i][j] = 0;
                }
            }
        }

        for (int i = 1; i <= steps.length; i++) {
            for (int j = 1; j <= n; j++) {
                if (steps[i - 1] <= j) {
                    dp[i][j] = dp[i][j - steps[i - 1]] + dp[i - 1][j];
                } else {
                    dp[i][j] = dp[i - 1][j];
                }
            }
        }

        return dp[steps.length][n];
    }

    // Here, we found min number of steps to be taken to reach top.
    private static int minSteps(int[][] dp, int n, int[] steps) {
        for (int i = 0; i <= steps.length; i++) {
            for (int j = 0; j <= n; j++) {
                if (i == 0) {
                    dp[i][j] = Integer.MAX_VALUE;
                } else if (j == 0) {
                    dp[i][j] = 0;
                }
            }
        }

        int k = 1;
        for (int j = 1; j <= n; j++) {
            if (j % steps[k - 1] == 0) {
                dp[k][j] = j / steps[k - 1];
            } else {
                dp[k][j] = Integer.MAX_VALUE - 1;
            }
        }
        for (int i = 2; i <= steps.length; i++) {
            for (int j = 1; j <= n; j++) {

                if (steps[i - 1] <= j) {
                    dp[i][j] = Math.min(1 + dp[i][j - steps[i - 1]], dp[i - 1][j]);
                } else {
                    dp[i][j] = dp[i - 1][j];
                }
            }
        }

        for (int i = 2; i <= steps.length; i++) {
            for (int j = 1; j <= n; j++) {

                System.out.print(dp[i][j] + " ");
            }
            System.out.println();
        }

        return dp[steps.length][n];
    }
}

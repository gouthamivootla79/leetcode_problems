package org.dp.practice.DP_ON_SUBSEQUENCE;

public class MaxSumInSubset {
    public static void main(String[] args) {
//        int[] nums = {-5, 3, -2, 4};
        int[] nums = {-2,1,3,-4,3};
        System.out.println(maxSum(nums, nums.length - 1));
    }

    private static int maxSum(int[] nums, int n) {

        if(n==0){
            return Math.max(0,nums[0]);
        }

        int pick = nums[n] + maxSum(nums,n-1);
        int notPick = maxSum(nums,n-1);

        return Math.max(pick,notPick);
    }
}

package org.dp.practice.DP_ON_SUBSEQUENCE;

public class Subset_SubsetSumEqualToTarget {
    public static void main(String[] args) {
        int[] nums = {5, 3, 2, 4};
        int total = 6;
        Boolean[][] dp = new Boolean[nums.length][total + 1];

        System.out.println(subsetSum(nums, nums.length - 1, total));
        System.out.println(subsetSum(nums, nums.length - 1, total, dp));
        System.out.println(subsetSumBottomUp(nums, nums.length - 1, total));
        System.out.println(subsetSumBottomUpSpaceOptimisation(nums, nums.length - 1, total));
    }

    // Recursive
    private static boolean subsetSum(int[] nums, int n, int total) {
        if (total == 0)
            return true;

        if (n == 0) {
            if (nums[0] == total) return true;
            else return false;
        }

        boolean pick = false;
        if (nums[n] <= total)
            pick = subsetSum(nums, n - 1, total - nums[n]);
        boolean notPick = subsetSum(nums, n - 1, total);

        return pick | notPick;
    }

    //Memoization
    private static boolean subsetSum(int[] nums, int n, int total, Boolean[][] dp) {
        if (total == 0)
            return true;

        if (n == 0) {
            if (nums[0] == total) return true;
            else return false;
        }

        if (dp[n][total] != null)
            return dp[n][total];

        boolean pick = false;
        if (nums[n] <= total)
            pick = subsetSum(nums, n - 1, total - nums[n]);
        boolean notPick = subsetSum(nums, n - 1, total);

        return dp[n][total] = pick | notPick;
    }

    //Bottom Up
    private static boolean subsetSumBottomUp(int[] nums, int n, int total) {
        Boolean[][] dp = new Boolean[nums.length][total + 1];

        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j <= total; j++) {
                if (j == 0)
                    dp[i][j] = true;
                else if (i == 0) {
                    if (nums[i] == j)
                        dp[i][j] = true;
                    else
                        dp[i][j] = false;
                }
            }
        }

        for (int i = 1; i < nums.length; i++) {
            for (int j = 1; j <= total; j++) {

                boolean pick = false;
                if (nums[i] <= j)
                    pick = dp[i - 1][j - nums[i]];
                boolean notPick = dp[i - 1][j];

                dp[i][j] = pick | notPick;
            }
        }

        return dp[n][total];
    }

    //Bottom Up + Space Optimisation
    private static boolean subsetSumBottomUpSpaceOptimisation(int[] nums, int n, int total) {
        Boolean[] prevRow = new Boolean[total + 1];

        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j <= total; j++) {
                if (j == 0)
                    prevRow[j] = true;
                else if (i == 0) {
                    if (nums[i] == j)
                        prevRow[j] = true;
                    else
                        prevRow[j] = false;
                }
            }
        }

        for (int i = 1; i < nums.length; i++) {
            Boolean[] temp = new Boolean[total + 1];
            temp[0] = true;
            for (int j = 1; j <= total; j++) {

                boolean pick = false;
                if (nums[i] <= j)
                    pick = prevRow[j - nums[i]];
                boolean notPick = prevRow[j];

                temp[j] = pick | notPick;
            }
            prevRow = temp;
        }

        return prevRow[total];
    }
}

package org.dp.practice.DP_ON_SUBSEQUENCE;

/**
 * https://leetcode.com/problems/coin-change/description/
 */
public class CoinChange_MinCoinForAmount {
    public static void main(String[] args) {
        int[] coins = {1, 2, 5};
        int amount = 5;
        int n = coins.length;

        int val = minWaysRecursion(coins, n - 1, amount);
        System.out.println(val == Integer.MAX_VALUE - 1 ? -1 : val);

        System.out.println(minWaysBottomUp(coins, n - 1, amount));
        System.out.println(minWaysBottomUpSpaceOptimisation(coins, n - 1, amount));
    }

    //Recursion TC: O(2^n) even more , SC: O(n) even more -> stack space
    private static int minWaysRecursion(int[] coins, int n, int amount) {
        if (amount == 0)
            return 0;
        if (n == 0) {
            if (amount % coins[0] == 0)
                return amount / coins[0];
            else
                return Integer.MAX_VALUE - 1;
        }

        int pick = Integer.MAX_VALUE;
        if (coins[n] <= amount) {
            pick = 1 + minWaysRecursion(coins, n, amount - coins[n]);
        }
        int notPick = minWaysRecursion(coins, n - 1, amount);

        return Math.min(pick, notPick);
    }

    //BottomUp TC: O(m*n) , SC: O(m*n)
    private static int minWaysBottomUp(int[] coins, int n, int amount) {
        int[][] dp = new int[coins.length][amount + 1];

        for (int i = 0; i < coins.length; i++) {
            for (int j = 0; j <= amount; j++) {
                if (j == 0)
                    dp[i][j] = 0;
                else if (i == 0) {
                    if (j % coins[i] == 0)
                        dp[i][j] = j / coins[i];
                    else
                        dp[i][j] = Integer.MAX_VALUE - 1;
                }
            }
        }

        for (int i = 1; i < coins.length; i++) {
            for (int j = 1; j <= amount; j++) {

                int pick = Integer.MAX_VALUE;
                if (coins[i] <= j) {
                    pick = 1 + dp[i][j - coins[i]];
                }
                int notPick = dp[i - 1][j];

                dp[i][j] = Math.min(pick, notPick);
            }
        }

        if (dp[n][amount] == Integer.MAX_VALUE - 1)
            return -1;
        else
            return dp[n][amount];
    }

    //BottomUp + Space Optimisation TC: O(m*n) , SC: O(n)
    private static int minWaysBottomUpSpaceOptimisation(int[] coins, int n, int amount) {
        int[] prevRow = new int[amount + 1];

        for (int i = 0; i < coins.length; i++) {
            for (int j = 0; j <= amount; j++) {
                if (j == 0)
                    prevRow[j] = 0;
                else if (i == 0) {
                    if (j % coins[i] == 0)
                        prevRow[j] = j / coins[i];
                    else
                        prevRow[j] = Integer.MAX_VALUE - 1;
                }
            }
        }

        for (int i = 1; i < coins.length; i++) {
            int[] temp = new int[amount + 1];
            temp[0] = 0;
            for (int j = 1; j <= amount; j++) {

                int pick = Integer.MAX_VALUE;
                if (coins[i] <= j) {
                    pick = 1 + temp[j - coins[i]];
                }
                int notPick = prevRow[j];

                temp[j] = Math.min(pick, notPick);
            }
            prevRow = temp;
        }

        if (prevRow[amount] == Integer.MAX_VALUE - 1)
            return -1;
        else
            return prevRow[amount];
    }
}

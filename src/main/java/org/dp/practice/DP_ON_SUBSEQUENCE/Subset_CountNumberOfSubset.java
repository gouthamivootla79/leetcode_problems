package org.dp.practice.DP_ON_SUBSEQUENCE;

import java.util.Arrays;

/*
An array is given
we have to find total no. of subsets with given sum k
 */
public class Subset_CountNumberOfSubset {
    public static void main(String[] args) {
        int[] nums = {2, 3, 1, 4};
        int sum = 5;
        int[][] dp = new int[nums.length][sum + 1];
        for (int i = 0; i < nums.length; i++)
            Arrays.fill(dp[i], -1);

        System.out.println(countSubset(nums, nums.length - 1, sum));
        System.out.println(countSubsetMemoization(nums, nums.length - 1, sum, dp));
        System.out.println(countSubsetBottomUp(nums, nums.length - 1, sum));
        System.out.println(countSubsetBottomUpSpaceOptimisation(nums, nums.length - 1, sum));
    }

    //Recursion TC: O(2^n) , SC: O(n) -> stack space
    private static int countSubset(int[] nums, int n, int total) {

        if (total == 0)
            return 1;
        if (n == 0) {
            if (nums[0] == total)
                return 1;
            else
                return 0;
        }

        int pick = 0;
        if (nums[n] <= total)
            pick = countSubset(nums, n - 1, total - nums[n]);
        int notPick = countSubset(nums, n - 1, total);

        return pick + notPick;
    }

    //Memoization TC: O(m*n) , SC: O(m*n) + O(n)-> stack space
    private static int countSubsetMemoization(int[] nums, int n, int total, int[][] dp) {

        if (total == 0)
            return 1;
        if (n == 0) {
            if (nums[0] == total)
                return 1;
            else
                return 0;
        }

        if (dp[n][total] != -1) return dp[n][total];

        int pick = 0;
        if (nums[n] <= total)
            pick = countSubset(nums, n - 1, total - nums[n]);
        int notPick = countSubset(nums, n - 1, total);

        return dp[n][total] = pick + notPick;
    }

    //BottomUp TC: O(m*n) , SC: O(m*n)
    private static int countSubsetBottomUp(int[] nums, int n, int total) {

        int[][] dp = new int[nums.length][total + 1];

        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j <= total; j++) {
                if (j == 0) {
                    dp[i][j] = 1;
                } else if (i == 0) {
                    if (nums[i] == j)
                        dp[i][j] = 1;
                    else
                        dp[i][j] = 0;
                }
            }
        }

        for (int i = 1; i < nums.length; i++) {
            for (int j = 1; j <= total; j++) {
                int pick = 0;
                if (nums[i] <= j)
                    pick = dp[i - 1][j - nums[i]];
                int notPick = dp[i - 1][j];

                dp[i][j] = pick + notPick;
            }
        }

        return dp[n][total];
    }

    //BottomUp + Space Optimisation, TC: O(m*n) , SC: O(n)
    private static int countSubsetBottomUpSpaceOptimisation(int[] nums, int n, int total) {

        int[] prevRow = new int[total + 1];

        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j <= total; j++) {
                if (j == 0) {
                    prevRow[j] = 1;
                } else if (i == 0) {
                    if (nums[i] == j)
                        prevRow[j] = 1;
                    else
                        prevRow[j] = 0;
                }
            }
        }

        for (int i = 1; i < nums.length; i++) {
            int[] temp = new int[total + 1];
            temp[0] = 1;
            for (int j = 1; j <= total; j++) {
                int pick = 0;
                if (nums[i] <= j)
                    pick = prevRow[j - nums[i]];
                int notPick = prevRow[j];

                temp[j] = pick + notPick;
            }
            prevRow = temp;
        }


        return prevRow[total];
    }
}

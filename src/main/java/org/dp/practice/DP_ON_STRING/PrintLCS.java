package org.dp.practice.DP_ON_STRING;

public class PrintLCS {
    public static void main(String[] args) {
        String str01 = "abcba";
        String str02 = "abcbcba";
        int m = str01.length();
        int n = str02.length();

        System.out.println(lcsBottomUp02(str01, str02, m, n));
    }

    private static int lcsBottomUp02(String str01, String str02, int m, int n) {
        int[][] dp = new int[m + 1][n + 1];

        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 0;
                }
            }
        }

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {

                int pick = 0;
                int notPick01 = 0;
                int notPick02 = 0;
                if (str01.charAt(i - 1) == str02.charAt(j - 1)) {
                    pick = 1;
                    if ((i - 1) >= 0 && (j - 1) >= 0)
                        pick += dp[i - 1][j - 1];
                }
                if ((i - 1) >= 0)
                    notPick01 = dp[i - 1][j];
                if ((j - 1) >= 0)
                    notPick02 = dp[i][j - 1];

                dp[i][j] = Math.max(pick, Math.max(notPick01, notPick02));
            }
        }

        printLCS(dp,m,n,str01,str02);
        return dp[m][n];
    }

    //TC: O(m+n) at worst case , SC: O(1)
    private static void printLCS(int[][] dp, int m, int n, String str01, String str02) {

        while(m>0 && n>0){

            if(str01.charAt(m-1)== str02.charAt(n-1)){
                System.out.print(str01.charAt(m-1) + " ");
                m--;
                n--;
            }else if(dp[m-1][n]> dp[m][n-1]){
                m--;
            }else{
                n--;
            }
        }
        System.out.println();
    }

}

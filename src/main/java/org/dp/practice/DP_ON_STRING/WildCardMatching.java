package org.dp.practice.DP_ON_STRING;

/**
 * Return true/false if string "s" matches with pattern "p"
 * '?' -> 1 element match
 * '*' -> 0 or more element match
 * https://leetcode.com/problems/wildcard-matching/description/
 */
public class WildCardMatching {
    public static void main(String[] args) {
        String s = "horse";
        String p = "*or?e";
        int m = s.length();
        int n = p.length();

        Boolean[][] dp = new Boolean[m+1][n+1];

        System.out.println(isMatchMemoization(s, p, m - 1, n - 1, dp));
        System.out.println(isMatchBottomUp(s,p));
        System.out.println(isMatchBottomUpSpaceOptimisation(s,p));
    }

    //Memoization O(m*n) , SC: O(m*n) + O(m+n) -> stack space
    private static boolean isMatchMemoization(String s, String p, int m, int n,Boolean[][] dp){

        if(n<0 && m<0) return true;
        if(n<0 && m>=0) return false;
        if(m<0 && n>=0){
            for(int i=n;i>=0;i--){
                if(p.charAt(i)!='*'){
                    return false;
                }
            }
            return true;
        }

        if(dp[m][n] != null) return dp[m][n];

        if(s.charAt(m) == p.charAt(n) || p.charAt(n) =='?'){
            return dp[m][n] = isMatchMemoization(s,p,m-1,n-1,dp);
        }else if( p.charAt(n) =='*'){
            return dp[m][n] = isMatchMemoization(s,p,m,n-1,dp) | isMatchMemoization(s,p,m-1,n,dp);
        }else
            return dp[m][n] = false;
    }

    //BottomUp TC: O(m*n) , SC: O(m*n)
    private static boolean isMatchBottomUp(String s, String p){

        int m = s.length();
        int n = p.length();
        Boolean[][] dp = new Boolean[m+1][n+1];

        dp[0][0] = true;
        for(int i=1;i<=m;i++)
            dp[i][0] = false;

        for(int j=1;j<=n;j++){
            boolean flag = true;
            for(int k=1;k>=j;k--){
                if(p.charAt(k-1)!='*'){
                    flag =false;
                    break;
                }
            }
            dp[0][j] = flag ;
        }

        for(int i=1;i<=m;i++){
            for(int j=1;j<=n;j++){

                if(s.charAt(i-1) == p.charAt(j-1) || p.charAt(j-1) =='?'){
                    dp[i][j] = dp[i-1][j-1];
                }else if( p.charAt(j-1) =='*'){
                    dp[i][j] = dp[i][j-1] | dp[i-1][j];
                }else
                    dp[i][j] = false;
            }
        }

        return dp[m][n];
    }

    //BottomUp + Space Optimisation, Approach 01 TC: O(m*n) , SC: O(n)..
    private static boolean isMatchBottomUpSpaceOptimisation(String s, String p){

        int m = s.length();
        int n = p.length();
        Boolean[] prevRow = new Boolean[n+1];

        prevRow[0] = true;
//        for(int i=1;i<=m;i++)
//            dp[i][0] = false;

        for(int j=1;j<=n;j++){
            boolean flag = true;
            for(int k=1;k>=j;k--){
                if(p.charAt(k-1)!='*'){
                    flag =false;
                    break;
                }
            }
            prevRow[j] = flag ;
        }

        for(int i=1;i<=m;i++){
            Boolean[] temp = new Boolean[n+1];
            temp[0] =  false;
            for(int j=1;j<=n;j++){

                if(s.charAt(i-1) == p.charAt(j-1) || p.charAt(j-1) =='?'){
                    temp[j] = prevRow[j-1];
                }else if( p.charAt(j-1) =='*'){
                    temp[j] = temp[j-1] | prevRow[j];
                }else
                    temp[j] = false;
            }
            prevRow =  temp;
        }

        return prevRow[n];
    }
}

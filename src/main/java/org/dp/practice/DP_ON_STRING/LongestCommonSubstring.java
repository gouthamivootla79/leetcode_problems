package org.dp.practice.DP_ON_STRING;

public class LongestCommonSubstring {
    public static void main(String[] args) {
        String str01 = "abcba";
        String str02 = "abcbcba";
        int m = str01.length();
        int n = str02.length();

        System.out.println(lcsubstringBottomUp02(str01,str02,m,n));
    }

    //BottomUp
    //TC: O(m*n) , SC: O(m*n)
    private static int lcsubstringBottomUp02(String str01, String str02, int m, int n) {
        int[][] dp = new int[m + 1][n + 1];

        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 0;
                }
            }
        }

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {

                int pick = 0;
                int notPick01 = 0;
                int notPick02 = 0;
                if (str01.charAt(i - 1) == str02.charAt(j - 1)) {
                    pick = 1 + dp[i - 1][j - 1];
                }else{
                    //Here we wont be picking from the left m-1 or n-1 chars, if prev char is not matched. It should be continuous
                    notPick01 = 0;
                    notPick02 = 0;

                    dp[i][j] = 0;
                }

                dp[i][j] = pick;
            }
        }

        // Finding the max value in 2D Array
        int lcs = 0;
        for(int i=0;i<str01.length()+1;i++){
            for(int j=0;j<str02.length()+1;j++){
                lcs = Math.max(lcs,dp[i][j]);
            }
        }

        return lcs;
    }
}

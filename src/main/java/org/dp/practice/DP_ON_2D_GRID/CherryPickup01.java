package org.dp.practice.DP_ON_2D_GRID;

/*
Leetcode : https://leetcode.com/problems/cherry-pickup/description/
You are given an n x n grid representing a field of cherries, each cell is one of three possible integers.

0 means the cell is empty, so you can pass through,
1 means the cell contains a cherry that you can pick up and pass through, or
-1 means the cell contains a thorn that blocks your way.
Return the maximum number of cherries you can collect by following the rules below:

Starting at the position (0, 0) and reaching (n - 1, n - 1) by moving right or down through valid path cells (cells with value 0 or 1).
After reaching (n - 1, n - 1), returning to (0, 0) by moving left or up through valid path cells.
When passing through a path cell containing a cherry, you pick it up, and the cell becomes an empty cell 0.
If there is no valid path between (0, 0) and (n - 1, n - 1), then no cherries can be collected.
 */
public class CherryPickup01 {

    public static void main(String[] args) {
        int[][] grid = {{0, 1, -1},
                {1, 0, -1},
                {1, 1, 1}};

        int r = grid.length;
        int c = grid[0].length;
        Integer[][][][] dp = new Integer[r][c][r][c];

        int max = maxPathMemoization(0,0,0,0,grid,dp);
        System.out.println( max>=0 ? max : 0);
    }

    //Memoization
    private static int maxPathMemoization(int r1,int c1,int r2,int c2, int[][] grid,Integer[][][][] dp){

        // BC- Outbound & Obstacle
        if(r1 >= grid.length || r2 >= grid.length || c1<0 || c2<0 || c1 >=grid[0].length || c2 >=grid[0].length || grid[r1][c1]==-1|| grid[r2][c2]==-1){
            return Integer.MIN_VALUE;
        }

        // BC
        if(r1 ==grid.length-1 && c1 == grid[0].length-1 )
            return grid[r1][c1];

        if(dp[r1][c1][r2][c2] != null)
            return dp[r1][c1][r2][c2];

        int cherry = 0;
        if(r1 == r2 && c1==c2){
            cherry = grid[r1][c1];
        }else{
            cherry = grid[r1][c1] + grid[r2][c2];
        }

        int dd= cherry +  maxPathMemoization(r1+1,c1,r2+1,c2,grid,dp);
        int dr= cherry +  maxPathMemoization(r1+1,c1,r2,c2+1,grid,dp);
        int rd= cherry +  maxPathMemoization(r1,c1+1,r2+1,c2,grid,dp);
        int rr= cherry +  maxPathMemoization(r1,c1+1,r2,c2+1,grid,dp);

        int max = Math.max(Math.max(dd,dr),Math.max(rd,rr));

        return dp[r1][c1][r2][c2] = max;
    }
}

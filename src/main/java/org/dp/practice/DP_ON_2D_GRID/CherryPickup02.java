package org.dp.practice.DP_ON_2D_GRID;

import java.util.Arrays;

/*
You are given a rows x cols matrix grid representing a field of cherries where grid[i][j] represents the number of cherries that you can collect from the (i, j) cell.

You have two robots that can collect cherries for you:

Robot #1 is located at the top-left corner (0, 0), and
Robot #2 is located at the top-right corner (0, cols - 1).
Return the maximum number of cherries collection using both robots by following the rules below:

From a cell (i, j), robots can move to cell (i + 1, j - 1), (i + 1, j), or (i + 1, j + 1).
When any robot passes through a cell, It picks up all cherries, and the cell becomes an empty cell.
When both robots stay in the same cell, only one takes the cherries.
Both robots cannot move outside of the grid at any moment.
Both robots should reach the bottom row in grid.
 */
public class CherryPickup02 {
    public static void main(String[] args) {
        int[][] grid = {{5, 10, 0},
                {65, 1, 40},
                {6, 34, 3}};

        int row = grid.length;
        int col = grid[0].length;
        int[][][] dp = new int[row][col][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                Arrays.fill(dp[i][j], -1);
            }
        }

        System.out.println(maxPathRecursion(0, 0, col - 1, grid));
        System.out.println(maxPathMemoization(0, 0, col - 1, grid, dp));
        System.out.println(maxPathBottomUp(row - 1, 0, col - 1, grid));
        System.out.println(maxPathBottomUpSpaceOptimisation(row - 1, 0, col - 1, grid));
    }

    // Recursion
    private static int maxPathRecursion(int row, int col01, int col02, int[][] grid) {


        if (col01 < 0 || col02 < 0 || col01 >= grid[0].length || col02 >= grid[0].length)
            return Integer.MIN_VALUE;

        if (row == grid.length - 1) {
            if (col01 == col02)
                return grid[row][col01];
            else
                return grid[row][col01] + grid[row][col02];
        }

        int maxi = Integer.MIN_VALUE;
        for (int j1 = -1; j1 <= +1; j1++) {
            for (int j2 = -1; j2 <= +1; j2++) {
                int value = 0;
                if (col01 == col02)
                    value = grid[row][col01];
                else
                    value = grid[row][col01] + grid[row][col02];

                value += maxPathRecursion(row + 1, col01 + j1, col02 + j2, grid);
                maxi = Math.max(maxi, value);
            }
        }

        return maxi;
    }

    // Memoization
    private static int maxPathMemoization(int row, int col01, int col02, int[][] grid, int[][][] dp) {


        if (col01 < 0 || col02 < 0 || col01 >= grid[0].length || col02 >= grid[0].length)
            return Integer.MIN_VALUE;

        if (row == grid.length - 1) {
            if (col01 == col02)
                return grid[row][col01];
            else
                return grid[row][col01] + grid[row][col02];
        }

        if (dp[row][col01][col02] != -1)
            return dp[row][col01][col02];

        int maxi = Integer.MIN_VALUE;
        for (int j1 = -1; j1 <= +1; j1++) {
            for (int j2 = -1; j2 <= +1; j2++) {
                int value = 0;
                if (col01 == col02)
                    value = grid[row][col01];
                else
                    value = grid[row][col01] + grid[row][col02];

                value += maxPathMemoization(row + 1, col01 + j1, col02 + j2, grid, dp);
                maxi = Math.max(maxi, value);
            }
        }

        return dp[row][col01][col02] = maxi;
    }

    // Tabulation : BottomUp
    private static int maxPathBottomUp(int row, int col01, int col02, int[][] grid) {
        int r = grid.length;
        int c = grid[0].length;
        int[][][] dp = new int[r][c][c];

        //BC
        for (int j1 = 0; j1 < c; j1++) {
            for (int j2 = 0; j2 < c; j2++) {
                if (j1 == j2)
                    dp[row][j1][j2] = grid[row][j1];
                else
                    dp[row][j1][j2] = grid[row][j1] + grid[row][j2];
            }
        }

        for (int i = row - 1; i >= 0; i--) {
            for (int j1 = 0; j1 < c; j1++) {
                for (int j2 = 0; j2 < c; j2++) {

                    int maxi = Integer.MIN_VALUE;
                    for (int jd1 = -1; jd1 <= +1; jd1++) {
                        for (int jd2 = -1; jd2 <= +1; jd2++) {
                            int value = 0;
                            if (j1 == j2)
                                value = grid[i][j1];
                            else
                                value = grid[i][j1] + grid[i][j2];

                            if ((j1 + jd1) >= 0 && (j2 + jd2) >= 0 && (j1 + jd1) <= c - 1 && (j2 + jd2) <= c - 1) {
                                value += dp[i + 1][j1 + jd1][j2 + jd2];
                            } else
                                value += Integer.MIN_VALUE;

                            maxi = Math.max(maxi, value);
                        }
                    }

                    dp[i][j1][j2] = maxi;
                }
            }
        }

        return dp[0][col01][col02];
    }

    // Tabulation : BottomUp + SpaceOptimisation
    private static int maxPathBottomUpSpaceOptimisation(int row, int col01, int col02, int[][] grid) {
        int r = grid.length;
        int c = grid[0].length;
        int[][] front = new int[c][c];


        //BC
        for (int j1 = 0; j1 < c; j1++) {
            for (int j2 = 0; j2 < c; j2++) {
                if (j1 == j2)
                    front[j1][j2] = grid[row][j1];
                else
                    front[j1][j2] = grid[row][j1] + grid[row][j2];
            }
        }

        for (int i = row - 1; i >= 0; i--) {
            int[][] temp = new int[c][c];

            for (int j1 = 0; j1 < c; j1++) {
                for (int j2 = 0; j2 < c; j2++) {

                    int maxi = Integer.MIN_VALUE;
                    for (int jd1 = -1; jd1 <= +1; jd1++) {
                        for (int jd2 = -1; jd2 <= +1; jd2++) {
                            int value = 0;
                            if (j1 == j2)
                                value = grid[i][j1];
                            else
                                value = grid[i][j1] + grid[i][j2];

                            if ((j1 + jd1) >= 0 && (j2 + jd2) >= 0 && (j1 + jd1) <= c - 1 && (j2 + jd2) <= c - 1) {
                                value += front[j1 + jd1][j2 + jd2];
                            } else
                                value += Integer.MIN_VALUE;

                            maxi = Math.max(maxi, value);
                        }
                    }

                    temp[j1][j2] = maxi;
                }
            }

            front = temp;
        }

        return front[col01][col02];
    }
}

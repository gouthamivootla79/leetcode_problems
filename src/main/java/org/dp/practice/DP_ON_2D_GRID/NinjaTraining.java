package org.dp.practice.DP_ON_2D_GRID;

import java.util.Arrays;

public class NinjaTraining {
    public static void main(String[] args) {

        int[][] price = {{2, 1, 3},
                {3, 4, 6},
                {10, 1, 3},
                {8, 3, 7}};

        int days = price.length;
        int last = 4;

        int[][] dp = new int[days][last];
        int[][] newDp = new int[days][last];
        for (int i = 0; i < days; i++) {
            Arrays.fill(dp[i], -1);
        }

        System.out.println(maxSum(price, days - 1, last - 1));
        System.out.println(maxSum(price, days - 1, last - 1, dp));
        System.out.println(maxSumBottomUp(price, days - 1, last - 1, newDp));
        System.out.println(maxSumBottomUpWithSpaceOptimisation(price, days - 1, last - 1));
    }

    //Recursive Solution
    private static int maxSum(int[][] price, int days, int last) {

        if (days == 0) {
            int max = Integer.MIN_VALUE;

            for (int task = 0; task <= 2; task++) {
                if (task != last) {
                    max = Math.max(max, price[days][task]);
                }
            }
            return max;
        }

        int max = Integer.MIN_VALUE;
        int points = 0;
        for (int task = 0; task <= 2; task++) {
            if (task != last) {
                points = price[days][task] + maxSum(price, days - 1, task);
            }
            max = Math.max(max, points);
        }

        return max;
    }

    //Memoization Top-Down
    private static int maxSum(int[][] price, int days, int last, int[][] dp) {
        if (days == 0) {
            int max = Integer.MIN_VALUE;

            for (int task = 0; task <= 2; task++) {
                if (task != last) {
                    max = Math.max(max, price[days][task]);
                }
            }
            return dp[days][last] = max;
        }

        if (dp[days][last] != -1)
            return dp[days][last];

        int max = Integer.MIN_VALUE;
        int points = 0;
        for (int task = 0; task <= 2; task++) {
            if (task != last) {
                points = price[days][task] + maxSum(price, days - 1, task);
            }
            max = Math.max(max, points);
        }

        return dp[days][last] = max;
    }

    //Tabulation Bottom Up
    private static int maxSumBottomUp(int[][] price, int days, int last, int[][] dp) {

        int day = 0;
        for (int j = 0; j <= last; j++) {

            int max = Integer.MIN_VALUE;
            for (int task = 0; task <= 2; task++) {
                if (task != j) {
                    max = Math.max(max, price[day][task]);
                }
            }
            dp[day][j] = max;
        }


        for (int i = 1; i <= days; i++) {
            for (int j = 0; j <= last; j++) {

                int max = Integer.MIN_VALUE;
                int points = 0;
                for (int task = 0; task <= 2; task++) {
                    if (task != j) {
                        points = price[i][task] + dp[i-1][task];
                    }
                    max = Math.max(max,points);
                }
                dp[i][j] = max;
            }
        }

        return dp[days][last];
    }

    //Tabulation Bottom Up With Space Optimisation
    private static int maxSumBottomUpWithSpaceOptimisation(int[][] price, int days, int last) {

        int[] prev = new int[last+1];
        int day = 0;
        for (int j = 0; j <= last; j++) {

            int max = Integer.MIN_VALUE;
            for (int task = 0; task <= 2; task++) {
                if (task != j) {
                    max = Math.max(max, price[day][task]);
                }
            }
            prev[j] = max;
        }


        for (int i = 1; i <= days; i++) {
            int[] temp = new int[last+1];
            for (int j = 0; j <= last; j++) {

                int max = Integer.MIN_VALUE;
                int points = 0;
                for (int task = 0; task <= 2; task++) {
                    if (task != j) {
                        points = price[i][task] + prev[task];
                    }
                    max = Math.max(max,points);
                }
                temp[j] = max;
            }

            prev = temp;
        }

        return prev[last];
    }
}

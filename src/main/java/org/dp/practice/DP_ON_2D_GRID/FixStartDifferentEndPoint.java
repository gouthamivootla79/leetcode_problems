package org.dp.practice.DP_ON_2D_GRID;

import java.util.Arrays;

/*
An array of Triangle is given
start point is (0,0)
end point is End row.
Condition : From (0,0) we can down or diagonal-right in down direction
Find the min path sum to reach at lowest row.
 */
public class FixStartDifferentEndPoint {
    public static void main(String[] args) {
        int[][] grid = {{5, -1, -1},
                {65, 1, -1},
                {6, 34, 3}};

        int row = grid.length;
        int col = grid[0].length;

        int[][] dp = new int[row][col];
        int[][] newDp = new int[row][col];
        for (int i = 0; i < row; i++)
            Arrays.fill(dp[i], -1);

        System.out.println(minPathRecursion(0, 0, grid));
        System.out.println(minPathMemoization(0, 0, grid, dp));
        System.out.println(minPathBottomUp(row - 1, col - 1, grid, newDp));
        System.out.println(minPathBottomUpSpaceOptimisation(row - 1, col - 1, grid));
    }

    //Recursion
    private static int minPathRecursion(int row, int col, int[][] grid) {

        if (row == grid.length - 1) {
            return grid[row][col];
        }

        int diag = grid[row][col] + minPathRecursion(row + 1, col + 1, grid);
        int down = grid[row][col] + minPathRecursion(row + 1, col, grid);

        return Math.min(diag, down);
    }

    //Memoization
    private static int minPathMemoization(int row, int col, int[][] grid, int[][] dp) {

        if (row == grid.length - 1) {
            return grid[row][col];
        }
        if (dp[row][col] != -1)
            return dp[row][col];

        int diag = grid[row][col] + minPathMemoization(row + 1, col + 1, grid, dp);
        int down = grid[row][col] + minPathMemoization(row + 1, col, grid, dp);

        return dp[row][col] = Math.min(diag, down);
    }

    //Tabulation Bottom Up
    private static int minPathBottomUp(int row, int col, int[][] grid, int[][] dp) {

        for (int j = 0; j <= col; j++) {
            dp[row][j] = grid[row][j];
        }

        for (int i = row-1; i >= 0; i--) {
            for (int j = i ; j >= 0; j--) {

                int diag = grid[row][col] + dp[i + 1][j + 1];
                int down = grid[row][col] + dp[i + 1][j];

                dp[i][j] = Math.min(diag, down);
            }
        }
        for (int i = 0; i <= grid.length-1; i++)
            System.out.println(Arrays.toString(dp[i]));

        return dp[0][0];
    }

    //Tabulation Bottom Up + Space Optimisation
    private static int minPathBottomUpSpaceOptimisation(int row, int col, int[][] grid) {
        int[] befRow = new int[col+1];

        for (int j = 0; j <= col; j++) {
            befRow[j] = grid[row][j];
        }

        for (int i = row-1; i >= 0; i--) {
            int[] temp = new int[i+1];
            for (int j = i ; j >= 0; j--) {

                int diag = grid[row][col] + befRow[j + 1];
                int down = grid[row][col] + befRow[j];

                temp[j] = Math.min(diag, down);
            }
            befRow = temp;
        }

        System.out.println(Arrays.toString(befRow));

        return befRow[0];
    }
}

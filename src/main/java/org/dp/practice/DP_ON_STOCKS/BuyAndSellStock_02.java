package org.dp.practice.DP_ON_STOCKS;

import java.util.Arrays;

/**
 * price[] is given, to buy and sell stock for each day
 * user can buy and sell >1s,
 * user can buy again, only if he/she has sell prev stock
 * Find max profit
 */
public class BuyAndSellStock_02 {
    public static void main(String[] args) {
        int[] price = {7, 1, 5, 3, 6, 4};
        //buy =1 -> available for buy
        //buy =0 -> not available for buy
        int buy = 1;
        int index = 0;
        int[][] dp = new int[price.length][2];
        for (int[] arr : dp) {
            Arrays.fill(arr, -1);
        }

        System.out.println(maxProfit(price, index, buy));
        System.out.println(maxProfit_TopDown(price, index, buy, dp));
        System.out.println(maxProfit_BottomUp(price));
        System.out.println(maxProfit_BottomUp_SpaceOptimisation(price));
    }

    //Recursive Approach
    private static int maxProfit(int[] price, int index, int buy) {

        //length of stock reached , thus 0 will be returned and can not be buy/sell
        if (index == price.length)
            return 0;

        int profit = 0;

        //Stock available for buying, has 2 choices - either to buy or not buy
        // "-" will be added, if stock bought
        if (buy == 1) {
            profit = Math.max(-price[index] + maxProfit(price, index + 1, 0),
                    maxProfit(price, index + 1, 1));
        } else {
            //Stock available for selling, has 2 choices - either to sell or not sell
            // "+" will be added, if stock is sell

            profit = Math.max(price[index] + maxProfit(price, index + 1, 1),
                    maxProfit(price, index + 1, 0));
        }

        return profit;
    }

    //Memoization - TopDown
    private static int maxProfit_TopDown(int[] price, int index, int buy, int[][] dp) {

        //length of stock reached , thus 0 will be returned and can not be buy/sell
        if (index == price.length)
            return 0;

        if (dp[index][buy] != -1)
            return dp[index][buy];

        int profit;

        //Stock available for buying, has 2 choices - either to buy or not buy
        // "-" will be added, if stock bought
        if (buy == 1) {
            profit = Math.max(-price[index] + maxProfit(price, index + 1, 0),
                    maxProfit(price, index + 1, 1));
        } else {
            //Stock available for selling, has 2 choices - either to sell or not sell
            // "+" will be added, if stock is sell

            profit = Math.max(price[index] + maxProfit(price, index + 1, 1),
                    maxProfit(price, index + 1, 0));
        }

        return dp[index][buy] = profit;
    }

    //Bottom up Approach
    private static int maxProfit_BottomUp(int[] price) {
        int[][] dp = new int[price.length][2];
        dp[price.length-1][0] = 0;
        dp[price.length-1][1] = 0;

        for (int i = price.length - 2; i >= 0; i--) {
            for (int j = 0; j < 2; j++) {
                int profit ;

                if (j == 1) {
                    profit = Math.max(-price[i] + dp[i + 1][0], dp[i + 1][1]);
                } else {
                    profit = Math.max(price[i] + dp[i + 1][1], dp[i + 1][0]);
                }

                dp[i][j] = profit;
            }
        }

        return dp[0][1];
    }

    //Bottom up Approach + Space optimisatiion
    private static int maxProfit_BottomUp_SpaceOptimisation(int[] price) {
        int[] prev = new int[2];
        prev[0] = 0;
        prev[1] = 0;

        for (int i = price.length - 2; i >= 0; i--) {
            int[] temp = new int[2];
            for (int j = 0; j < 2; j++) {
                int profit ;

                if (j == 1) {
                    profit = Math.max(-price[i] + prev[0], prev[1]);
                } else {
                    profit = Math.max(price[i] + prev[1], prev[0]);
                }

                temp[j] = profit;
            }
            prev = temp;
        }

        return prev[1];
    }
}

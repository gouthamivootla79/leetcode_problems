package org.dp.practice.DP_ON_STOCKS;

public class BuyAndSellStock_03 {
    public static void main(String[] args) {
        int[] price = {7, 1, 5, 3, 6, 4};
        //buy =1 -> available for buy
        //buy =0 -> not available for buy
        int buy = 1;
        int index = 0;
        int transaction = 1;

        System.out.println(maxProfit(price, index, buy, transaction));
    }

    //Recursive Approach
    private static int maxProfit(int[] price, int index, int buy, int t) {
        // IMPORTANT : BC WHEN TRANSACTION IS COMPLETE
        if (t == 0)
            return 0;

        //length of stock reached , thus 0 will be returned and can not be buy/sell
        if (index == price.length)
            return 0;

        int profit = 0;

        //Stock available for buying, has 2 choices - either to buy or not buy
        // "-" will be added, if stock bought
        if (buy == 1) {
            profit = Math.max(-price[index] + maxProfit(price, index + 1, 0, t),
                    maxProfit(price, index + 1, 1, t));
        } else {
            //Stock available for selling, has 2 choices - either to sell or not sell
            // "+" will be added, if stock is sell
            // IMPORTANT : THERE WILL BE CHANGE HERE, AS AFTER SELLING, A TRANSACTION GETS COMPLETE
            profit = Math.max(price[index] + maxProfit(price, index + 1, 1, t - 1),
                    maxProfit(price, index + 1, 0, t));
        }

        return profit;
    }
}

package org.dp.mcm;

import java.util.Arrays;

public class PalindromePartitioningMemoization {
    public static void main(String[] args) {
        String s = "cabababcbc";
        int[][] dp = new int[s.length()+1][s.length()+1];
        int[][] palindrome = new int[s.length()+1][s.length()+1];

        for(int[] arr:dp){
            Arrays.fill(arr,-1);
        }
        for(int[] arr:palindrome){
            Arrays.fill(arr,-1);
        }

        System.out.println("Min number of partition : " + palindromePartitionTopDown(s, 0, s.length() - 1,dp,palindrome));
        System.out.println("Min number of partition : " + minPalPartion(s, 0, s.length() - 1,dp));
    }

    private static int palindromePartitionTopDown(String s, int i, int j, int[][] dp, int[][] palindrome) {
        if(i>=j)
            return 0;
        if(dp[i][j]!=-1)
            return dp[i][j];
        if(palindrome[i][j]!=-1)
            return 0;

        if(isPalindrome(s,i,j)){
            palindrome[i][j]=1;
            return 0;
        }
        int min = Integer.MAX_VALUE;
        for(int k=i;k<=j-1;k++){
            if(dp[i][k]==-1){
                dp[i][k] = palindromePartitionTopDown(s, i, k, dp, palindrome);
            }
            if(dp[k+1][j]==-1){
                dp[k+1][j] = palindromePartitionTopDown(s, k+1, j, dp, palindrome);
            }

            int temp = 1+ dp[i][k] + dp[k+1][j];
            min = Math.min(min,temp);
        }
        dp[i][j] = min;
        return min;
    }

    //optimised solution
    private static int minPalPartion(String string, int i, int j, int[][] dp){
        if( i >= j || isPalindrome(string, i, j) )
            return 0;

        if(dp[i][j]!=-1){
            return dp[i][j];
        }
        dp[i][j] = Integer.MAX_VALUE;
        for(int k = i; k < j; k++){
            if(isPalindrome(string,i,k)){
                int count = Math.min(dp[i][j], 0 + minPalPartion(string, k + 1, j,dp) + 1);

                dp[i][j]=count;
            }
        }
        return dp[i][j];
    }

    private static boolean isPalindrome(String s, int i, int j) {
        if(i>=j)
            return true;
        if(s.charAt(i)!=s.charAt(j)){
            return false;
        }
        return isPalindrome(s,i+1,j-1);
    }
}

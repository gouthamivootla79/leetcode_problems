package org.dp.mcm;

import java.util.Arrays;

public class MCMBTopDown {
    public static void main(String[] args) {
        int[] arr ={2,3,4,2};
        int[][] dp =new int[args.length+10][args.length+10];
        for(int[] dpArr : dp){
            Arrays.fill(dpArr,-1);
        }
        System.out.println("Minimum no. of multiplication are : "+ mcm(arr, 1, arr.length - 1,dp));
    }

    private static int mcm(int[] arr, int i, int j,int[][] dp) {
        if(i>=j)
            return 0;
        if(dp[i][j]!=-1)
            return dp[i][j];
        int min = Integer.MAX_VALUE;
        int temp;
        for (int k=i;k<=j-1;k++){
            temp = mcm(arr, i, k,dp) + mcm(arr, k+1, j,dp) + arr[i-1]*arr[k]*arr[j];
            min= Math.min(min,temp);
        }

        dp[i][j] = min;
        return min;
    }
}

package org.dp.lcs;

/**
 * Two string is given , we have to find String a is subsequence of string b
 * String x = "abc";
 * String y = "aerbc";
 * if(lcs == x.length() return true
 * else return false
 */
public class SequencePatternMatching {
    public static void main(String[] args) {
        String x = "abc";
        String y = "aerbc";

        int[][] dp = new int[x.length() + 1][y.length() + 1];

        int lcs = findLCS(dp, x, y);

        if(lcs == x.length())
            System.out.println("It is subsequence");
        else
            System.out.println("It is not subsequence");
    }

    private static int findLCS(int[][] dp, String x, String y) {

        int xLength = x.length();
        int yLength = y.length();
        char[] xArr = x.toCharArray();
        char[] yArr = y.toCharArray();

        for (int i = 0; i <= xLength; i++) {
            for (int j = 0; j <= yLength; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 0;
                }
            }
        }

        for (int i = 1; i <= xLength; i++) {
            for (int j = 1; j <= yLength; j++) {
                if(xArr[i-1]==yArr[j-1]){
                    dp[i][j]= 1 + dp[i-1][j-1];
                }else{
                    dp[i][j]= Math.max(dp[i-1][j],dp[i][j-1]);
                }
            }
        }

        return dp[xLength][yLength];
    }
}

package org.dp.lcs;

/**
 * Two string are given , we have to find min number of Deletions and Insertion required to change String a -> b
 * x- heap
 * y- pea
 * LCS -> ea - 2
 * System.out.println("Deletion : " + (x.length() - lcs));
 * System.out.println("Insertion : " + (y.length() - lcs));
 */
public class MinNumberOfInsertionDeletion {
    public static void main(String[] args) {
        String x = "heap";
        String y = "pea";

        int[][] dp = new int[x.length() + 1][y.length() + 1];

        int lcs = findLCS(dp, x, y);
        System.out.println("Deletion : " + (x.length() - lcs));
        System.out.println("Insertion : " + (y.length() - lcs));
    }

    private static int findLCS(int[][] dp, String x, String y) {

        int xLength = x.length();
        int yLength = y.length();
        char[] xArr = x.toCharArray();
        char[] yArr = y.toCharArray();

        for (int i = 0; i <= xLength; i++) {
            for (int j = 0; j <= yLength; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 0;
                }
            }
        }

        for (int i = 1; i <= xLength; i++) {
            for (int j = 1; j <= yLength; j++) {
                if(xArr[i-1]==yArr[j-1]){
                    dp[i][j]= 1 + dp[i-1][j-1];
                }else {
                    dp[i][j]= Math.max(dp[i-1][j],dp[i][j-1]);
                }
            }
        }

        // Finding the max value in 2D Array
        int lcs = 0;
        for(int i=0;i<xLength+1;i++){
            for(int j=0;j<yLength+1;j++){
                lcs = Math.max(lcs,dp[i][j]);
            }
        }
        return lcs;

    }
}

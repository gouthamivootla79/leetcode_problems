package org.dp.knapsack;

/**
 * Print count of subbset if in an array its subset sum is equal to k.
 * Array values should be +ive
 */
public class CountSubsetSumDp {
    public static void main(String[] args) {
        int[] ints = {1, 4, 2, 8, 3, 6};
        int sum = 6;
        int[][] dp = new int[ints.length + 1][sum + 1];
        System.out.println(countSubsetSum(ints, sum,dp));
    }

    private static int countSubsetSum(int[] arr, int sum, int[][] dp) {
        int n = arr.length;
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= sum; j++) {
                if (i == 0 && j == 0) {
                    dp[i][j] = 1;
                } else {
                    if (j == 0) {
                        dp[i][j] = 1;
                    } else if (i == 0) {
                        dp[i][j] = 0;
                    }

                }
            }
        }

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= sum; j++) {
                if (arr[i - 1] <= j) {
                    dp[i][j] = dp[i - 1][j - arr[i - 1]] + dp[i - 1][j];
                } else {
                    dp[i][j] = dp[i - 1][j];
                }
            }
        }
        return dp[n][sum];
    }
}

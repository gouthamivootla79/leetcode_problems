package org.dp.knapsack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Find the min subset difference from an array.
 */
public class MinSubsetDifference {
    public static void main(String[] args) {
        int totalSum = 0;
        int[] arr = {1, 5, 8, 3};
        for(int ele :arr){
            totalSum = totalSum + ele;
        }
        System.out.println("Total sum : " + totalSum);
        boolean[][] dp = new boolean[arr.length + 1][totalSum + 1];
        minSubsetDiff(arr,dp, totalSum);
    }

    private static void minSubsetDiff(int[] arr, boolean[][] dp, int sum) {
        subsetSum(arr,dp, sum);
        int length;
        if(sum%2==0){
            length = sum/2;
        }else{
            length =sum/2 +1;
        }
        boolean[] value = Arrays.copyOf(dp[arr.length],length);
        for(boolean val  : value){
            System.out.print(val + " , ");
        }
        List<Integer> list  = new ArrayList<>();
        for(int i=0; i<value.length;i++){
            if(value[i] == true){
                list.add(i);
            }
        }
        int maxEle = list.parallelStream()
                 .sorted().collect(Collectors.toList()).get(list.size()-1);
        System.out.println("minSubsetDiff : " +  (sum - (2*maxEle)));
    }

    private static void subsetSum(int[] arr, boolean[][] dp, int sum) {
        int n = arr.length;
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= sum; j++) {
                if (i == 0 && j == 0) {
                    dp[i][j] = true;
                } else {
                    if (j == 0) {
                        dp[i][j] = true;
                    } else if (i == 0) {
                        dp[i][j] = false;
                    }

                }
            }
        }

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= sum; j++) {
                if (arr[i - 1] <= j) {
                    dp[i][j] = dp[i - 1][j - arr[i - 1]] | dp[i - 1][j];
                } else {
                    dp[i][j] = dp[i - 1][j];
                }
            }
        }
    }


}

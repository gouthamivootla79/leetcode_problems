package org.dp.knapsack;

/**
 * Coin array is given with different coins.
 * An amount is given. There are different ways using the coins an amount can be matched.
 * We can use same coin more than 1 time.
 * Here , we have to find min number of coins needed to match the amount.
 */
public class CoinChangeVersion02 {
    public static void main(String[] args) {
        int[] coinArr = {1, 2, 3};
        int amount = 5;
        int[][] dp = new int[coinArr.length + 1][amount + 1];

        System.out.println(minNumberOfCoinNeeded(coinArr, dp, amount));
    }

    private static int minNumberOfCoinNeeded(int[] coinArr, int[][] dp, int amount) {
        int n = coinArr.length;
        for(int i=0;i<=n;i++){
            for(int j=0;j<=amount;j++){
                if(i==0){
                    dp[i][j]= Integer.MAX_VALUE-1;
                }else if(j==0){
                    dp[i][j]= 0;
                }
            }
        }

        int index=1;
        for(int j=1;j<=amount;j++){
            if(j%coinArr[index-1]==0) {
                dp[index][j] = j / coinArr[index - 1];
            }else{
                dp[index][j]= Integer.MAX_VALUE-1;
            }
        }

        for(int i=2;i<=n;i++) {
            for (int j = 1; j <= amount; j++) {
                if(coinArr[i-1]<=j){
                    dp[i][j]= Math.min(1 + dp[i][j-coinArr[i-1]] , dp[i-1][j]);
                }else{
                    dp[i][j]= dp[i-1][j];
                }
            }
        }

        return dp[n][amount];
    }
}

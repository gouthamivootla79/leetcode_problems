package org.dp.knapsack;

/**
 * In UnboundedKnapsack, we can put any item in bag n-no. of times or can choose not to pick.
 * While in  0-1 Knapsack, an item can be picked only 1 time, or can choosen not to pick.
 */
public class UnboundedKnapsack {
    public static void main(String[] args) {
        int[] wt = {1, 2, 3, 4, 5};
        int[] val = {4, 2, 4, 5, 3};
        int totalWeight = 7;
        int[][] dp = new int[wt.length+1][totalWeight+1];
        System.out.println("Max profit : "+solveUnboundedKnapsack(wt, val, totalWeight, dp));
    }

    private static int solveUnboundedKnapsack(int[] wt, int[] val, int totalWeight, int[][] dp) {
        int n = wt.length;
        for(int i=0;i<=n;i++){
            for(int j=0;j<=totalWeight;j++){
                if(i==0 || j==0){
                    dp[i][j] = 0;
                }
            }
        }

        for(int i=1;i<=n;i++) {
            for (int j = 1; j <= totalWeight; j++) {
                if(wt[i-1]<=j){
                    dp[i][j] = Math.max(val[n-1] + dp[i][j-wt[i-1]],dp[i-1][j]);
                }else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }

        return dp[n][totalWeight];
    }
}

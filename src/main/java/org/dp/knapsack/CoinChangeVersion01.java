package org.dp.knapsack;

/**
 * Coin array is given with different coins.
 * An amount is given. We have to find different ways using the coins an amount can be matched.
 * We can use same coin more than 1 time.
 */

/**
 * This problem is similar to count subset sum, but with a difference of unbounded domain.
 */
public class CoinChangeVersion01 {
    public static void main(String[] args) {
        int[] coinArr = {1, 2, 3};
        int amount = 5;
        int[][] dp = new int[coinArr.length + 1][amount + 1];
        System.out.println(totalWaysForAmount(coinArr, dp, amount));
    }

    private static int totalWaysForAmount(int[] coinArr, int[][] dp, int amount) {
        int n = coinArr.length;
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= amount; j++) {
                if (i == 0 && j == 0) {
                    dp[i][j] = 1;
                } else if (j == 0) {
                    dp[i][j] = 1;
                } else if (i == 0) {
                    dp[i][j] = 0;
                }
            }
        }

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= amount; j++) {
                if (coinArr[i - 1] <= j) {
                    dp[i][j] = dp[i][j - coinArr[i - 1]] + dp[i - 1][j];
                } else {
                    dp[i][j] = dp[i - 1][j];
                }
            }
        }

        return dp[n][amount];
    }
}

package org.dp.knapsack;

import java.util.Arrays;
// Not working
/**
 * Print count of subbset if in an array its subset product is equal to k.
 * Array values should be +ive
 */
public class CountSubsetProductMemoizatiion {
    public static void main(String[] args) {
        int[] ints = {1,4,2,8,3,6};
        double prod= 8;
        int totalProd =1;
        int[][] dp= new int[ints.length+1][55];
        for(int[] arr: dp){
            Arrays.fill(arr,-1);
        }
        System.out.println(countSubsetProduct(ints, prod, dp,totalProd,ints.length));
    }

    private static int countSubsetProduct(int[] arr, double prod, int[][] dp, int totalProd, int n) {
        if(totalProd==prod){
            return 1;
        }else if(n==0){
            return 0;
        }
        if(dp[n][totalProd] !=-1){
            return dp[n][totalProd];
        }

        dp[n][totalProd] = countSubsetProduct(arr, prod, dp, totalProd*arr[n-1], n-1) +
                           countSubsetProduct(arr, prod, dp, totalProd, n-1);

        return dp[n][totalProd];
    }
}

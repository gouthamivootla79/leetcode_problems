package org.dp.knapsack;

/**
 * Count no. of subsets with given difference K.
 */
public class CountSubsetDifference {
    public static void main(String[] args) {
        int[] arr = {1, 2, 5};
        int totalSum =0;
        for(int ele :arr){
            totalSum = totalSum + ele;
        }
        int diff = 2;
        int sum = (totalSum+diff)/2;
        int[][] dp = new int[arr.length + 1][sum + 1];
        System.out.println(countSubsetSum(arr, sum, dp));
    }

    private static int countSubsetSum(int[] arr, int sum, int[][] dp) {
        int n = arr.length;
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= sum; j++) {
                if (i == 0 && j == 0) {
                    dp[i][j] = 1;
                } else {
                    if (j == 0) {
                        dp[i][j] = 1;
                    } else if (i == 0) {
                        dp[i][j] = 0;
                    }

                }
            }
        }

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= sum; j++) {
                if (arr[i - 1] <= j) {
                    dp[i][j] = dp[i - 1][j - arr[i - 1]] + dp[i - 1][j];
                } else {
                    dp[i][j] = dp[i - 1][j];
                }
            }
        }
        return dp[n][sum];
    }
}

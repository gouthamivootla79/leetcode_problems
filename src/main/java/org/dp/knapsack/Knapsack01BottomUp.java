package org.dp.knapsack;

public class Knapsack01BottomUp {
    static int[][] dp = new int[50][50];

    public static void main(String[] args) {
        Integer[] wt = {1, 2, 3, 4, 5};
        Integer[] val = {4, 2, 4, 5, 3};
        int totalWeight = 7;
        System.out.println("Knapsack : Max profit : " + solveKnapsack(totalWeight, wt, val));
    }

    private static int solveKnapsack(int w, Integer[] wt, Integer[] vt) {
        int n = wt.length;
        for (int i = 0; i <= (n + 1); i++) {
            for (int j = 0; j < (w + 1); j++) {
                if (i == 0 || j == 0)
                    dp[i][j] = 0;
            }
        }

        for (int i = 1; i < (n + 1); i++) {
            for (int j = 1; j < (w + 1); j++) {
                if (wt[i - 1] <= j) {
                    dp[i][j] = Math.max(vt[i - 1] + dp[i - 1][j - wt[i - 1]], dp[i - 1][j]);
                } else {
                    dp[i][j] = dp[i - 1][j];
                }
            }
        }
        return dp[n][w];
    }
}

package org.graph;

/**
 * A tree is given, convert to Double Linked List
 * Make sure, SC : O(1), no additional data structure should be used
 * Here, did it using Morris Traversal algorithm
 */
public class ConvertTreeToDLL {

    static class TreeNode {
        int data;
        TreeNode left;
        TreeNode right;

        TreeNode(int data, TreeNode left, TreeNode right) {
            this.data = data;
            this.left = left;
            this.right = right;
        }
    }

    public static void main(String[] args) {
        TreeNode node01 = new TreeNode(2,null,null);
        TreeNode node02 = new TreeNode(3,null,null);
        TreeNode node03 = new TreeNode(1,null,null);
        TreeNode node04 = new TreeNode(5,null,null);
        TreeNode node05 = new TreeNode(4,null,null);

        node03.left = node01;
        node01.right = node02;
        node03.right = node05;
        node05.left = node04;

        convert(node03);
    }

    // Morris Traversal
    // It should follow preorder -> rt, left, right
    // Create 2 var, cur -> root(as in preorder, it starts from root), prev -> curr.left (as in preorder, after root, left node is picked)
    private static void convert(TreeNode node03) {

        TreeNode curr = node03;
        TreeNode prev;

        // run it to whole tree, uptil, curr pointer marks to null
        while(curr != null){

            if(curr.left != null){

                //get the rightmost node from left subtree and assign it right to curr.right
                // and make curr right as curr left
                prev = curr.left;
                while(prev.right!=null){
                    prev = prev.right;
                }

                prev.right = curr.right;
                curr.right = curr.left;
            }
            // increment curr pointer to its right node, to which it was changed in before step - 57th step
            curr = curr.right;
        }

        // printing the value
        while(node03!=null){
            System.out.print("-" + node03.data + "-");
            node03 = node03.right;
        }
    }
}

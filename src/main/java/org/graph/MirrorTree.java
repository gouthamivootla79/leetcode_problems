package org.graph;

/**
 * Mirror Tree is also called Symmetric tree
 * It means when we devide tree from mid, its left subtree, should exactly match with right subtree
 * leftNode.left == rightNode.right
 * leftNode.right == rightNode.left
 */
public class MirrorTree {
    static class TreeNode {
        int data;
        TreeNode left;
        TreeNode right;

        TreeNode(int data, TreeNode left, TreeNode right) {
            this.data = data;
            this.left = left;
            this.right = right;
        }
    }

    public static void main(String[] args) {

        TreeNode node01 = new TreeNode(1, null, null);
        TreeNode node02 = new TreeNode(2, null, null);
        TreeNode node03 = new TreeNode(3, null, null);
        TreeNode node04 = new TreeNode(1, null, null);
        TreeNode node05 = new TreeNode(2, null, null);

        node03.left = node01;
        node03.right = node04;
        node01.right = node02;
        node04.left = node05;

        System.out.println(isMirrorTree(node03.left, node03.right));
    }

    private static boolean isMirrorTree(TreeNode left, TreeNode right) {
        if (left == null || right == null)
            return left == right;

        if (left.data != right.data)
            return false;

        return isMirrorTree(left.right, right.left) && isMirrorTree(left.left, right.right);
    }
}

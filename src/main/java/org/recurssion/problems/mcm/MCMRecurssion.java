package org.recurssion.problems.mcm;

public class MCMRecurssion {
    public static void main(String[] args) {
        int[] arr ={2,3,4,2};
        System.out.println("Minimum no. of multiplication are : "+ mcm(arr, 1, arr.length - 1));
    }

    private static int mcm(int[] arr, int i, int j) {
        if(i>=j)
            return 0;

        int min = Integer.MAX_VALUE;
        int temp;
        for (int k=i;k<=j-1;k++){
            temp = mcm(arr, i, k) + mcm(arr, k+1, j) + arr[i-1]*arr[k]*arr[j];
            min= Math.min(min,temp);
        }
        return min;
    }
}

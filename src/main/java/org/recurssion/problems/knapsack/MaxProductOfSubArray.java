package org.recurssion.problems.knapsack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MaxProductOfSubArray {
    public static void main(String[] args) {
        Integer[] intArray = {-2, -3, 1, 4, 5};
        List<Integer> list = new ArrayList<>();
        Collections.addAll(list, intArray);
        int product = 1;
        List<Integer> listProduct = new ArrayList<>();
        maxProduct(list, product, listProduct);
        List<Integer> sortedListProduct = listProduct.stream().sorted().collect(Collectors.toList());
        System.out.println(sortedListProduct);
        System.out.println(sortedListProduct.get(sortedListProduct.size() - 1));
        System.out.println("Max prod : "+ maxProduct(intArray,product,intArray.length));
    }

    private static void maxProduct(List<Integer> list, int product, List<Integer> listProduct) {
        if (list.size() == 0) {
            listProduct.add(product);
            return;
        }

        int prod01 = product * list.get(0);
        int prod02 = product;
        list.remove(0);
        maxProduct(list, prod01, listProduct);
        maxProduct(list, prod02, listProduct);
    }

    private static int maxProduct(Integer[] arr, int prod, int n){
        if(n==0){
            return prod;
        }

        return Math.max(maxProduct(arr,prod * arr[n-1],n-1),maxProduct(arr,prod,n-1));
    }
}

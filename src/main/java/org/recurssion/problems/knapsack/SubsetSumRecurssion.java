package org.recurssion.problems.knapsack;

/**
 * Print true/false if in an array its subset sum is equal to k.
 * Array values should be +ive
 */
class SubsetSumRecurssion {
    public static void main(String[] args) {
        int[] ints = {1,4,2,8,3,6};
        int sum= 10;

        System.out.println(subsetSum(ints, sum, ints.length));
    }

    private static boolean subsetSum(int[] arr, int sum, int n) {

        if(sum ==0){
            return true;
        }else if(n==0){
            return false;
        }

        if(arr[n-1]<=sum){
            return subsetSum(arr,sum-arr[n-1],n-1) | subsetSum(arr,sum,n-1);
        }else{
            return subsetSum(arr,sum,n-1);
        }
    }
}

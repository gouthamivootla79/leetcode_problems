package org.recurssion.problems.knapsack;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Find the min subset difference from an array.
 */
public class MinSubsetDifference {
    public static void main(String[] args) {
        int[] arr = {1, 5, 8, 3};
        int sum=0;
        int totalSum =0;
        for(int ele:arr){
            totalSum+=ele;
        }
        List<Integer> list = new ArrayList<>();
        subsetSum(arr,sum,list,arr.length);
        int finalTotalSum = totalSum;
        int minsubsetDiff= list.parallelStream().map(ele -> {
            int subset02 = finalTotalSum - ele;
            if(ele >= subset02){
                return ele - subset02;
            }else{
                return subset02-ele;
            }
        }).sorted().collect(Collectors.toList()).get(0);
        System.out.println("minsubsetDiff : " + minsubsetDiff);
    }

    private static void subsetSum(int[] arr, int sum, List<Integer> list, int n) {
        if(n==0){
            list.add(sum);
            return;
        }

        int sum01 = sum + arr[n-1];
        int sum02 = sum;

        subsetSum(arr, sum01, list, n-1);
        subsetSum(arr, sum02, list, n-1);
    }
}

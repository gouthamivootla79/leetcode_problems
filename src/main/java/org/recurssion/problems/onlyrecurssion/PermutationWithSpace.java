package org.recurssion.problems.onlyrecurssion;

public class PermutationWithSpace {
    public static void main(String[] args) {
        String str="abcd";
        String op=String.valueOf(str.charAt(0));
        permutationWithSpace(str.substring(1),op);

    }

    private static void permutationWithSpace(String str, String op) {
        if(str.length()==0){
            System.out.print("'"+op+"' ");
            return;
        }

        String op01 = op + str.charAt(0);
        String op02 = op + "_" + str.charAt(0);
        str = str.substring(1);
        permutationWithSpace(str, op01);
        permutationWithSpace(str,op02);
    }
}

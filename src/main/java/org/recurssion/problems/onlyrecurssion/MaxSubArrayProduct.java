package org.recurssion.problems.onlyrecurssion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MaxSubArrayProduct {
    public static void main(String[] args) {
        int[] arr = {-2, 1, 3, -4, -3};
        System.out.println(maxSubArrayProduct(arr, arr.length));


        ArrayList<Integer> arrList = new ArrayList<>();
        ArrayList<Integer> arrListResult = new ArrayList<>();
        Arrays.stream(arr).forEach(arrList::add);
        maxSubArrayProductUsingRecursion(arrListResult,arrList,1);

        System.out.println(arrListResult.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList()).get(0));

        arrListResult.sort(Comparator.reverseOrder());
        System.out.println(arrListResult.get(0));
    }

    private static int maxSubArrayProduct(int[] arr, int n) {
        int maxProd = arr[0];
        int maxProdEnd = arr[0];
        int minProdEnd = arr[0];
        int tempMax;

        for (int i = 1; i < n; i++) {
            tempMax = Math.max(arr[i], Math.max(maxProdEnd * arr[i], minProdEnd * arr[i]));
            minProdEnd = Math.min(arr[i], Math.min(maxProdEnd * arr[i], minProdEnd * arr[i]));
            maxProdEnd = tempMax;

            maxProd = Math.max(maxProd, maxProdEnd);
        }
        return maxProd;
    }

    private static void maxSubArrayProductUsingRecursion(List<Integer> list, List<Integer> arr,int value){
        if(arr.isEmpty()){
            list.add(value);
            return;
        }

        int prod01 = value * arr.get(arr.size()-1);
        int prod02 = value;
        arr.remove(arr.size()-1);
        maxSubArrayProductUsingRecursion(list,arr, prod01);
        maxSubArrayProductUsingRecursion(list,arr, prod02);
    }
}

package org.recurssion.problems.onlyrecurssion;

/**
 * Tower of Hanoi is a mathematical puzzle where we have three rods and n disks. The objective of the puzzle is to move the entire stack to another rod, obeying the following simple rules:
 * 1) Only one disk can be moved at a time.
 * 2) Each move consists of taking the upper disk from one of the stacks and placing it on top of another stack i.e. a disk can only be moved if it is the uppermost disk on a stack.
 * 3) No disk may be placed on top of a smaller disk.
 */
public class TowerOfHanoi {
    public static void main(String[] args) {
        int ring = 2;
        int count = 0;
        towerOFHanoi(ring, 'A', 'B', 'C', count);
    }

    private static void towerOFHanoi(int n, char a, char b, char c, int count) {
        count++;
        if (n == 1) {
            System.out.println("Move last ring from " + a + " -> " + c);
            return ;
        }
        System.out.println("Move " + n + "nd ring from " + a + " -> " + b);
        towerOFHanoi(n - 1, a, b, c, count);
        System.out.println("Move " + n + "rd ring from " + b + " -> " + c);
        towerOFHanoi(n - 1, b, c, a, count);
    }
}

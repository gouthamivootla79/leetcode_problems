package org.recurssion.problems.onlyrecurssion;

import java.util.ArrayList;
import java.util.List;

public class BalancedParentheses {
    public static void main(String[] args) {
        int n=3;
        List<String> list = new ArrayList<>();
        String op="";
        printBalancedParentheses(list,n,n,op);
        System.out.println(list);
    }

    private static void printBalancedParentheses(List<String> list, int ob, int cb, String op) {
        if(ob==0 && cb ==0){
            list.add(op);
            return;
        }

        if(ob!=0){
            ob--;
            op = op +"(";
            printBalancedParentheses(list,ob,cb,op);
        }

        if(cb>ob){
            cb--;
            op= op+")";
            printBalancedParentheses(list,ob,cb,op);
        }
    }
}

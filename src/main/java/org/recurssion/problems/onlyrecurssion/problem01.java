package org.recurssion.problems.onlyrecurssion;

import java.util.*;

public class problem01 {

    public static void main(String[] args) {

        int[][] graph={{1,3},{0,2},{1,3},{0,2}};
        int length = graph.length;
        System.out.println(length);
        System.out.println(isBipartite(graph));
    }

    public static boolean isBipartite(int[][] graph) {
        Map<Integer,Integer> integerMap= new HashMap<>();
        int length = graph.length;
        if (length%2!=0)
            return false;
        for (int i=0;i<length;i++){
            for(int j=i+1; j<length;j++){
                Arrays.sort(graph[i]);
                Arrays.sort(graph[j]);
                if(Arrays.equals(graph[i],graph[j])){
                    integerMap.put(i,j);
                }
            }
        }

        if(integerMap.size()==2)
            return true;
        else
            return false;
    }
}

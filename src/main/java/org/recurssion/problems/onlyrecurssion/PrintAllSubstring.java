package org.recurssion.problems.onlyrecurssion;

public class PrintAllSubstring {
    public static void main(String[] args) {
        String text = "abc";
        String op = "";
        String subStr = text;
        subString(text, subStr, op);
    }

    private static void subString(String text, String subStr, String op) {
        if (subStr.length() == 0) {
            System.out.print("'" + op + "'" + " ");
            return;
        }

        if (((text.indexOf(op) + op.length() ) == text.indexOf(subStr.charAt(0))) || op.equals("")) {
            String op01 = op + subStr.charAt(0);
            String op02 = op;
            subStr = subStr.substring(1);
            subString(text, subStr, op01);
            subString(text, subStr, op02);
        } else {
            String op01 = op;
            subStr = subStr.substring(1);
            subString(text, subStr, op01);
        }
    }
}

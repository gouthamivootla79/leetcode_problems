package org.recurssion.problems.onlyrecurssion;

//Running code
public class Problem03 {

    public static void main(String[] args) {
        int[] num = {1, 3, 5, 7};
        int target = 9;
        int posX = 0;
        int posY = num.length - 1;
        int value = findindex(num, posX, posY, target);
        System.out.println("Index is : " + value);
    }

    private static int findindex(int[] num, int posX, int posY, int target) {
        int lo = 0;
        int hi = num.length-1;
        if(target <= num[lo])
            return lo;
        if(target > num[hi])
            return hi+1;
        while (lo <= hi){
            int mid = lo + (hi - lo) / 2;
            if(num[mid]> target){
                hi = mid-1;
            }else if(num[mid]< target){
                lo = mid+1;
            }
        }
        return lo;
    }

}

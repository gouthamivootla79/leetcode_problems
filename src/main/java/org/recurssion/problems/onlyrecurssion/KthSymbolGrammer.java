package org.recurssion.problems.onlyrecurssion;

/**
 * Rule of grammer -> 1-10 ,0-01
 * Given row N, index K. Find the Kth indexed symbol in row N.
 * When N and K = 1, then value is 0.
 */
public class KthSymbolGrammer {

    public static void main(String[] args) {
        System.out.println(kthGrammar(2, 2));
    }

    public static int kthGrammar(int n, int k) {
        if (n == 1 && k == 1)
            return 0;

        int mid = (int) (Math.pow(2, n - 1) / 2);

        if (k <= mid)
            return kthGrammar(n - 1, k);
        else
            return 1 - kthGrammar(n - 1, k - mid);
    }
}

package org.recurssion.problems.onlyrecurssion;

import java.util.ArrayList;
import java.util.List;

public class Number1GreaterEqualNumber0Prefixes {
    public static void main(String[] args) {
        int n=5;
        String op ="";
        List<String> list = new ArrayList<>();
        printPrefixes(list,0,0,n,op,n);
        System.out.println(list);
    }

    private static void printPrefixes(List<String> list, int num1, int num0, int n, String op, int digit) {
        if(n==0){
            list.add(op);
            return;
        }

        if(num1!=digit){
            num1++;
            n--;
            op = op + "1";
            printPrefixes(list,num1,num0,n,op,digit);
        }

        if(num1>num0){
            num0++;
            n--;
            op=op+"0";
            printPrefixes(list,num1,num0,n,op,digit);
        }
    }
}

package org.recurssion.stack;

import java.util.Arrays;

class TwoStackArray{
    int[] arr;
    int size;
    int top01 ,top02;

    @Override
    public String toString() {
        return "StackArray{" +
                "arr=" + Arrays.toString(arr)+'}';
    }

    TwoStackArray(int size){
        this.size = size;
        arr = new int[size];
        top01 = -1;
        top02 = size;
    }

    public boolean isFull(){
        return (top01+1)==top02;
    }

    public boolean isEmpty01(){
        return (top01==-1);
    }

    public boolean isEmpty02(){
        return (top02 ==size);
    }

    public void push01(int value){
        if(isFull()) {
            System.out.println("Stack is full");
            return;
        }
        top01++;
        arr[top01]=value;
    }

    public void push02(int value){
        if(isFull()) {
            System.out.println("Stack is full");
            return;
        }
        top02--;
        arr[top02]=value;
    }

    public int pop01(){
        if(isEmpty01()) {
            System.out.println("Stack is Empty");
            return -1;
        }
        int top = arr[top01];
        arr[top01] = -1;
        top01--;
        return top;
    }

    public int pop02(){
        if(isEmpty02()) {
            System.out.println("Stack is Empty");
            return -1;
        }
        int top = arr[top02];
        arr[top02] = -1;
        top02++;
        return top;
    }

    public int peek01(){
        if(isEmpty01()) {
            System.out.println("Stack is Empty");
            return -1;
        }
        int top = arr[top01];
        return top;
    }

    public int peek02(){
        if(isEmpty01()) {
            System.out.println("Stack is Empty");
            return -1;
        }
        int top = arr[top02];
        return top;
    }
}

public class TwoStackUsingArray {

    public static void main(String[] args) {
        TwoStackArray stack = new TwoStackArray(5);
        stack.push01(1);
        stack.push01(3);
        stack.push01(2);

        stack.push02(4);
        stack.push02(5);
        stack.push02(7);

        System.out.println(stack);
        System.out.println(stack.peek01());
        System.out.println(stack.peek02());
        System.out.println(stack.pop01());
        System.out.println(stack.pop02());
        System.out.println(stack.peek01());
        System.out.println(stack.peek02());
        stack.push02(7);
        System.out.println(stack.peek02());
        System.out.println(stack);
    }
}

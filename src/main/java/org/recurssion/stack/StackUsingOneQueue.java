package org.recurssion.stack;

import java.util.ArrayDeque;
import java.util.Queue;

class StackOneQueue {
    Queue<Integer> q1;
    boolean flag = false;

    @Override
    public String toString() {
        return "StackOneQueue{" +
                "q1=" + q1 +
                '}';
    }

    StackOneQueue() {
        q1 = new ArrayDeque<>();
    }

    public void push(int value) {
        q1.add(value);
    }

    public int pop() {
        if(flag == false) {
            q1 = reverseQueue((ArrayDeque<Integer>) q1);
            flag= true;
        }
        int top= q1.peek();
        this.q1 = q1;
        this.q1.poll();
        return top;
    }

    private ArrayDeque<Integer> reverseQueue(ArrayDeque<Integer> queue) {
        if(queue.isEmpty()){
            return queue;
        }

        int top= queue.peek();
        queue.pop();
        queue =reverseQueue(queue);
        queue.add(top);
        return queue;
    }

    public int peek() {
        if(flag == false) {
            q1 = reverseQueue((ArrayDeque<Integer>) q1);
            flag= true;
        }
        int top= q1.peek();

        return top;
    }
}

public class StackUsingOneQueue {
    public static void main(String[] args) {
        StackOneQueue stack = new StackOneQueue();
        stack.push(1);
        stack.push(4);
        stack.push(7);
        stack.push(2);

        System.out.println(stack);
        System.out.println(stack.peek());
        System.out.println(stack.pop());
        System.out.println(stack);
    }
}

package org.recurssion.stack;

import java.util.Stack;

/**
 * Trick :
 * push() : if val>=max ? s.push(2*val - max), min= val : s.push(val)
 * pop() : top= s.peek() , if top>=max ? max = 2*max - top , s.pop()
 */
class StackMaxEleOneStack {
    Stack<Integer> s1;
    int max = -1;

    public StackMaxEleOneStack() {
        s1 = new Stack<>();
    }

    @Override
    public String toString() {
        return "StackMaxEleOneStack{" +
                "s1=" + s1 +
                '}';
    }

    public void push(int value) {
        if (s1.isEmpty()) {
            max = value;
            s1.push(value);
            return;
        }

        if (value >= max) {
            s1.push(2 * value - max);
            max = value;
        } else {
            s1.push(value);
        }
    }

    public int pop() {
        int top = s1.peek();
        if (top >= max) {
            int prevMax;
            prevMax = 2 * max - top;
            max = prevMax;
        }
        return s1.pop();
    }

    public int peek() {
        int top = s1.peek();
        if (top >= max) {
            return max;
        }
        return top;
    }

    public int maxEle() {
        return max;
    }
}

public class StackMaxEleConstTime_OneStack {

    public static void main(String[] args) {
        StackMaxEleOneStack stack = new StackMaxEleOneStack();
        stack.push(3);
        stack.push(1);
        stack.push(3);
        stack.push(2);
        stack.push(0);

        System.out.println(stack);
        System.out.println(stack.maxEle());
    }
}

package org.recurssion.stack;

import java.util.Stack;

public class ReverseStack {

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(3);
        stack.push(2);
        stack.push(1);
        System.out.println(stack);
        System.out.println(reverseStack(stack));
    }

    private static Stack<Integer> reverseStack(Stack<Integer> stack) {
        if(stack.isEmpty()){
            return stack;
        }
        int top = stack.peek();
        stack.pop();
        stack = reverseStack(stack);
        return pushValueAtBottom(stack,top);
    }

    private static Stack<Integer> pushValueAtBottom(Stack<Integer> stack,int value){
        if(stack.isEmpty()){
            stack.push(value);
            return stack;
        }
        int top = stack.peek();
        stack.pop();
        stack = pushValueAtBottom(stack,value);
        stack.push(top);
        return stack;
    }
}

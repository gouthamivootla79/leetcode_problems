package org.recurssion.stack;

import java.util.Stack;

class StackMinEleTwoStack {
    @Override
    public String toString() {
        return "StackMinEleTwoStack{" +
                "s1=" + s1 +
                '}';
    }

    Stack<Integer> s1;
    Stack<Integer> s2;

    public StackMinEleTwoStack() {
        s1 = new Stack<>(); // It will be taken for storing the volumes
        s2 = new Stack<>(); // It will be used for storing the min values from stack s1
    }

    public void push(int value) {
        if (s1.isEmpty()) {
            s1.push(value);
            s2.push(value);
            return;
        }
        s1.push(value);
        s2.push(Math.min(value,s2.peek()));
    }

    public int pop() {
        if(s1.isEmpty())
            return -1;
        s2.pop();
        return s1.pop();
    }

    public int peek() {
        return s1.peek();
    }

    public int minEle() {
        return s2.peek();
    }
}

public class StackMinEleConstTime_TwoStack {
    public static void main(String[] args) {
        StackMinEleTwoStack stack = new StackMinEleTwoStack();
        stack.push(3);
        stack.push(1);
        stack.push(2);
        stack.push(0);

        System.out.println(stack);
        System.out.println(stack.minEle());

        System.out.println(stack.peek());
        System.out.println(stack.pop());
        System.out.println(stack);
        System.out.println(stack.minEle());
    }
}

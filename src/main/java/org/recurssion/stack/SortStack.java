package org.recurssion.stack;

import java.util.Stack;

public class SortStack {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        stack.push(3);
        stack.push(2);
        stack.push(5);

        System.out.println(stack);

        stack = sortStack(stack);
        System.out.println("Sorted : " + stack);
    }

    private static Stack<Integer> sortStack(Stack<Integer> stack) {
        if (stack.isEmpty()) {
            return stack;
        }
        int top = stack.peek();
        stack.pop();
        stack = sortStack(stack);
        stack = placeValueInStack(stack, top);

        return stack;
    }

    private static Stack<Integer> placeValueInStack(Stack<Integer> stack, int targetValue) {
        if (stack.isEmpty()) {
            stack.push(targetValue);
            return stack;
        }

        int topElement = stack.peek();

        if (topElement < targetValue) {
            stack.pop();
            placeValueInStack(stack, targetValue);
            stack.push(topElement);
        } else {
            stack.push(targetValue);
        }

        return stack;
    }
}

package org.recurssion.stack;

import java.util.Stack;

/**
 * Trick :
 * push() : if val<=min ? s.push(2*val - min), min= val : s.push(val)
 * pop() : top= s.peek() , if top<=min ? min = 2*min - top , s.pop()
 */
class StackMinEleOneStack {
    Stack<Integer> s1;
    int min = -1;

    public StackMinEleOneStack() {
        s1 = new Stack<>();
    }

    @Override
    public String toString() {
        return "StackMinEleTwoStack{" +
                "s1=" + s1 +
                '}';
    }

    public void push(int value) {
        if (s1.isEmpty()) {
            s1.push(value);
            min = value;
        } else if (value <= min) {
            s1.push(2 * value - min);
            min = value;
        } else {
            s1.push(value);
        }
    }

    public int pop() {
        int top = s1.peek();
        if (top <= min) {
            int prevMin;
            prevMin = 2 * min - top;
            min = prevMin;
        }
        return s1.pop();
    }

    public int minEle() {
        return min;
    }
}

public class StackMinEleConstTime_OneStack {
    public static void main(String[] args) {
        StackMinEleOneStack stack = new StackMinEleOneStack();
        stack.push(3);
        stack.push(1);
        stack.push(3);
        stack.push(2);
        stack.push(0);

        System.out.println(stack);
        System.out.println(stack.minEle());

//        System.out.println(stack.peek());
        System.out.println(stack.pop());
        System.out.println(stack.minEle());
        System.out.println(stack);
        System.out.println(stack.pop());
        System.out.println(stack);
        System.out.println(stack.minEle());
    }
}

package org.recurssion.queue;

import java.util.Stack;

class QueueStack{
    Stack<Integer> s1;
    Stack<Integer> s2;

    QueueStack(){
        s1 = new Stack<>();
        s2 = new Stack<>();
    }

    @Override
    public String toString() {
        return "QueueStack{" +
                "s1=" + s1 +
                '}';
    }

    public void add(int value){
        int top;
        while (!s1.isEmpty()){
            top= s1.pop();
            s2.push(top);
        }

        s1.push(value);

        while (!s2.isEmpty()){
            top= s2.pop();
            s1.push(top);
        }
    }

    public int poll(){
        return s1.pop();
    }

    public int peek(){
        int top = s1.peek();
        return top;
    }
}
public class QueueUsingTwoStack {
    public static void main(String[] args) {
        QueueStack queue = new QueueStack();
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);

        System.out.println(queue);
        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println(queue);
    }
}

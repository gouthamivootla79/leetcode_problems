package org.recurssion.queue;

import java.util.Stack;

class QueueOneStack {
    Stack<Integer> s1;
    boolean flag = false;

    @Override
    public String toString() {
        return "QueueOneStack{" +
                "s1=" + s1 +
                '}';
    }

    QueueOneStack() {
        s1 = new Stack<>();
    }

    public void add(int value) {
        s1.push(value);
    }

    public int poll() {
        if (flag == false) {
            s1 = reverseStack(s1);
            flag = true;
        }
        int top = s1.pop();

        return top;
    }

    private Stack reverseStack(Stack<Integer> stack) {
        if (stack.isEmpty()) {
            return stack;
        }

        int top = stack.peek();
        stack.pop();
        stack = reverseStack(stack);

        stack = pushValueAtBottom(stack, top);

        return stack;
    }

    private Stack pushValueAtBottom(Stack<Integer> stack, int top) {
        if (stack.isEmpty()) {
            stack.push(top);
            return stack;
        }

        int top01 = stack.peek();
        stack.pop();
        stack = pushValueAtBottom(stack, top);
        stack.push(top01);
        return stack;
    }

    public int peek() {
        if (flag == false) {
            s1 = reverseStack(s1);
            flag = true;
        }
        int top = s1.peek();
        return top;
    }
}

public class QueueUsingOneStack {
    public static void main(String[] args) {
        QueueOneStack queue = new QueueOneStack();
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);

        System.out.println(queue);
        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println(queue);
    }
}

package org.contest.hackerrank;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * https://www.hackerrank.com/challenges/one-month-preparation-kit-sparse-arrays/problem
 */
public class SparseArray {

    public static List<Integer> matchingStrings(List<String> strings, List<String> queries) {
        // Write your code here
        Map<String, Integer> map = new LinkedHashMap<>();
        for (String q1 : queries) {
            map.put(q1, 0);
            for (String s1 : strings) {
                if (q1.equals(s1)) {
                    map.put(q1, map.get(q1) + 1);
                }
            }
        }

        List<Integer> sol = new ArrayList<Integer>();
        for (String i : queries)    //O(m)
            sol.add(map.getOrDefault(i, 0));

        return sol;
    }
}

package org.contest.hackerrank;

import java.io.*;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * https://www.enjoyalgorithms.com/blog/maximum-subarray-sum
 */

public class amazonPrime {
    public static void main(String[] args) {
        int[] arr = {2,5,6,11};
        System.out.println(solve(arr.length, arr));
    }

    static int solve(int N, int[] arr){
        // Write your code here
        if(N==0) return 0;

        int result = 0;
        Map<Integer,List<Integer>> map = new LinkedHashMap<>();
        List<Integer> setList  = new ArrayList<>();
        subset(arr, N,map,0, setList);
        System.out.println(map);
        List<Integer> valueList  = new ArrayList<>();

        for (Map.Entry<Integer,List<Integer>> entry : map.entrySet()) {
            List<Integer> list  = new ArrayList<>(entry.getValue());
            int length = list.size();

            if(length ==0){
                valueList.add(0);
                break;
            }
            double median;
            int sum =0;
            if(length > 1) {
                if (length % 2 == 0) {
                    median = (list.get((length / 2)-1) + list.get((length / 2))) / 2;
                } else {
                    median = list.get((length / 2)-1);
                }
            }else{
                if(length==1){
                    median = list.get(0);
                }else{
                    median = 0;
                }
            }

            for (Integer valuInteger : list) {
                sum = sum + valuInteger;
            }
            double mean  = sum/length;
            double value = 3*Math.abs(mean-median) ;
            valueList.add((int)value);
        }
        System.out.println(valueList.parallelStream()
                .sorted().collect(Collectors.toList()));

        result = valueList.parallelStream()
                .sorted().collect(Collectors.toList()).get(0);
        return result;

    }

    static void subset(int[] arr, int n, Map<Integer,List<Integer>> map, int i,List<Integer> list){

        if(n==0){
            double random =  Math.random()*123 + Math.random()*123;
            map.put((int)random, list);
            return ;
        }
        List<Integer> list01 = list.stream().collect(Collectors.toList());
        list01.add(arr[n-1]);
        List<Integer> list02 = list.stream().collect(Collectors.toList());
//        System.out.println(list01);
        subset(arr, n-1, map, i, list01);
        subset(arr, n-1, map, i, list02);
    }
}

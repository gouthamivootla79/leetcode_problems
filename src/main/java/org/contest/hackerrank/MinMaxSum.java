package org.contest.hackerrank;

import java.util.List;
import java.util.stream.Collectors;

/**
 * https://www.hackerrank.com/challenges/one-month-preparation-kit-mini-max-sum/problem
 */
public class MinMaxSum {

    public static void miniMaxSum(List<Integer> arr) {
        // Write your code here
        List<Integer> sortedList = arr.parallelStream()
                .sorted().collect(Collectors.toList());

        long min = 0;
        long max = 0;

        for (int i = 0; i < sortedList.size() - 1; i++) {
            min = min + sortedList.get(i);
            max = max + sortedList.get(sortedList.size() - 1 - i);
        }
        System.out.println(min + " " + max);
    }
}

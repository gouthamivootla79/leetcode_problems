package org.contest.hackerrank;

import java.util.ArrayList;
import java.util.List;

/**
 * https://www.hackerrank.com/challenges/one-month-preparation-kit-plus-minus/problem
 */
public class PlusMinus {

    public static void plusMinus(List<Integer> arr) {
        // Write your code here
        List<Integer> positiveList = new ArrayList<>();
        List<Integer> negativeList = new ArrayList<>();
        List<Integer> zerosList = new ArrayList<>();

        for (int ele : arr) {
            if (ele > 0) {
                positiveList.add(ele);
            } else if (ele < 0) {
                negativeList.add(ele);
            } else {
                zerosList.add(ele);
            }
        }
        Double dp = Double.valueOf(positiveList.size()) / Double.valueOf(arr.size());
        dp = Double.parseDouble(String.format("%.6f", dp));
        System.out.println(dp);

        Double dn = Double.valueOf(negativeList.size()) / Double.valueOf(arr.size());
        dn = Double.parseDouble(String.format("%.6f", dn));
        System.out.println(dn);

        Double dz = Double.valueOf(zerosList.size()) / Double.valueOf(arr.size());
        dz = Double.parseDouble(String.format("%.6f", dz));
        System.out.println(dz);

    }
}

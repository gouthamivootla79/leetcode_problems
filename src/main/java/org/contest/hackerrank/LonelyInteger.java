package org.contest.hackerrank;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * https://www.hackerrank.com/challenges/one-month-preparation-kit-lonely-integer/problem
 */
public class LonelyInteger {

    public static int lonelyinteger(List<Integer> a) {
        // Write your code here
        Map<Integer,Integer> map = new HashMap<>();
        for(int ele: a){
            map.merge(ele, 1, Integer::sum);
        }

        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue());
            if(entry.getValue()==1){
                return entry.getKey();
            }
        }

        return 0;

    }
}

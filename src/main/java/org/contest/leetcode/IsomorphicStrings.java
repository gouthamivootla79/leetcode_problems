package org.contest.leetcode;

import java.util.*;

public class IsomorphicStrings {

    public static void main(String[] args) {
        String s ="abcda";
        String t= "abcdb";

        System.out.println("\n"+isIsomorphic(s,t));
    }

    public static boolean isIsomorphic(String s, String t) {

        if(s.length() != t.length())
            return false;

        char[] sArr = s.toCharArray();
        char[] tArr = t.toCharArray();

        List<Integer> sList = new LinkedList<>();
        List<Integer> tList = new LinkedList<>();

        Map<Character,Integer> map01 = new LinkedHashMap<>();
        Map<Character,Integer> map02 = new LinkedHashMap<>();

        for(int i=0;i<sArr.length;i++){
            map01.merge(sArr[i],1,Integer::sum);
            map02.merge(tArr[i],1,Integer::sum);

            if(i!=0){
                if(sArr[i]!=sArr[i-1]){
                    sList.add(0);
                }
                if(tArr[i]!=tArr[i-1]){
                    tList.add(0);
                }
            }

            sList.add(map01.get(sArr[i]));
            tList.add(map02.get(tArr[i]));
        }

        System.out.println(map01);
        System.out.println(map02);

        sList.forEach(System.out::print);
        System.out.println("\n--------");
        tList.forEach(System.out::print);

        if(map01.size()!=map02.size()){
            return false;
        }

        int flag=0;

        List<Integer> sList01 = new LinkedList<>(map01.values());
        List<Integer> tList01 = new LinkedList<>(map02.values());

        return sList.equals(tList) && (sList01.equals(tList01));
    }
}

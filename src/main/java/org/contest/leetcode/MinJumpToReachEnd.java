package org.contest.leetcode;

import java.util.Arrays;

/**
 * An array of positive int is given, find the min no. of Jumps to reach the End.
 */
public class MinJumpToReachEnd {
    public static void main(String[] args) {
        int[] arr = {3,2,2,1,4,5};
        int[] jump = new int[arr.length];
        Arrays.fill(jump,-1);

        System.out.println("minJumpToReachEndRecursive : "+minJumpToReachEndRecursive(arr, 0, arr.length));
        System.out.println("minJumpToReachEndTopDown : "+minJumpToReachEndTopDown(arr, 0, arr.length,jump));
    }

    private static int minJumpToReachEndRecursive(int[] arr, int start, int end) {
        if(start >=end)
            return 0;

        int min = Integer.MAX_VALUE;
        for(int index =1;index <=arr[start] && index < end;index++){
            int countJump = 1 + minJumpToReachEndRecursive(arr, start + index,end);
            min = Math.min(min,countJump);
        }
        return min;
    }

    private static int minJumpToReachEndTopDown(int[] arr, int start, int end, int[] jump) {
        if(start >=end)
            return 0;
        if(jump[start]!=-1){
            return jump[start];
        }
        int min = Integer.MAX_VALUE;
        for(int index =1;index <=arr[start] && index < end;index++){
            int countJump = 1 + minJumpToReachEndTopDown(arr, start + index,end,jump);
            min = Math.min(min,countJump);
        }
        jump[start] = min;
        return min;
    }
}

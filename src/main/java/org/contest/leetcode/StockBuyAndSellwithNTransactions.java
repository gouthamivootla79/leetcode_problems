package org.contest.leetcode;

/**
 * https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/
 */
public class StockBuyAndSellwithNTransactions {
    public static void main(String[] args) {
        int[] prices = {7,1,5,3,6,4};
        int trans = 2;
        int[][] dp = new int[trans + 1][prices.length];
        System.out.println("Max profit : " + maxProfitAnyNTrans(prices, trans, dp));
    }

    private static int maxProfitAnyNTrans(int[] prices, int trans, int[][] dp) {
        int n1 = trans + 1;
        int n2 = prices.length;

        for (int i = 0; i < n1; i++) {
            for (int j = 0; j < n2; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 0;
                }
            }
        }

        int maxDiff = Integer.MIN_VALUE;
        for (int i = 1; i < n1; i++) {
            for (int j = 1; j < n2; j++) {
                maxDiff = Math.max(maxDiff, dp[i - 1][j - 1] - prices[j - 1]);
                dp[i][j] = Math.max(dp[i][j - 1], prices[j] + maxDiff);
            }
            maxDiff = Integer.MIN_VALUE;
        }

        for (int i = 0; i < n1; i++) {
            for (int j = 0; j < n2; j++) {
                System.out.print(dp[i][j] + " ");
            }
            System.out.println("");
        }

        return dp[n1-1][n2-1];
    }
}

package org.contest.leetcode;

/**
 * https://leetcode.com/problems/climbing-stairs/
 * https://www.enjoyalgorithms.com/blog/climbing-stairs-problem
 * https://www.techiedelight.com/find-total-ways-reach-nth-stair-with-atmost-m-steps/
 * Staircase with n steps is given , a person is allowed to take 1 0r 2 jump.
 * There are number of ways to reach top
 * Find the total number of ways to reach top of staircase.
 */
public class ClimbingStairs {
    public static void main(String[] args) {
        int n = 5;
        System.out.println("Recursive approach :" + climbStairsRecursive(n,3));
        System.out.println("Bottom up DP approach :" + climbStairsBottomUp(n,3));
    }

    //T.C = O(2^n)
    //S.C = O(n)
    private static int climbStairsRecursive(int n, int steps) {
        if (n<0)
            return 0;
        if (n==0)
            return 1;

        int value =0;
        for(int i=1;i<=steps;i++){
            value += climbStairsRecursive(n-i,steps);
        }

        return value;
    }

    //T.C = O(n)
    //S.C = O(n)
    private static int climbStairsBottomUp(int n,int steps) {
        int[] stair = new int[n + 1];
        stair[0] = 1;
        stair[1] = 1;
        stair[2] = 2;

        for (int i = 3; i <= n; i++) {
            stair[i] = 0;
            for(int j =1 ;j<=steps && i>=j;j++) {
                stair[i] += stair[i - j];
            }
        }
        return stair[n];
    }

    //T.C = O(n)
    //S.C = O(1)
    private static int climbStairsWithLessSC(int n) {
        if(n==0 || n==1){
            return n;
        }

        int n1 = 1;
        int n2 = 2;

        for (int i = 3; i <= n; i++) {
            int n3 = n1 + n2;
            n1 = n2;
            n2 = n3;
        }
        return n2;
    }
}

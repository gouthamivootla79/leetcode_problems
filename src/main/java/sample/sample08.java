package sample;

import java.util.*;

public class sample08 {

    public static void main(String[] args) {
        Map<Integer,String> build= new LinkedHashMap<>();

        build.put(1,"S");
        build.put(2,"S");
        build.put(3,"S");
        build.put(4,"S");
        build.put(5,"S");
        build.put(6,"S");
        build.put(7,"U");
        build.put(8,"U");
        build.put(9,"U");
        build.put(10,"U");

        for(Map.Entry<Integer,String> entry : build.entrySet()){
            if(entry.getValue().equals("U")){
                System.out.println("Unsuccessful Build is : " + entry.getKey());
                break;
            }
        }

        List<Integer> listBuild = new LinkedList<>();
        for(Map.Entry<Integer,String> entry : build.entrySet()){
            listBuild.add(entry.getKey());
        }
    }
}

package sample;

import java.util.ArrayList;
import java.util.List;

/**
 * WebPt : get a product array
 * 1 -> 2*3*4 = totalProd/1
 * 2 -> 1*3*4 = totalProd/2
 * 3 -> 1*2*4 = totalProd/3
 * 4 -> 1*2*3 = totalProd/4
 *
 * Get the product of all, and then divide it with value
 * ans : [24, 12, 8 ,6]
 */
public class sample02 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4};

        product(arr).stream()
                .forEach(System.out::println);

    }

    private static List<Integer> product(int[] arr){
        List<Integer> integerList = new ArrayList<>();

        for(int i=0;i< arr.length;i++){
            int prod = 1;
            for(int j =0 ; j< arr.length; j++){
                if(j!=i){
                    prod = prod * arr[j];
                }
            }
            integerList.add(prod);
        }

        return integerList;
    }
}

package sample;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * WebPt : print characters that are repeating in a string
 */
public class sample01 {
    public static void main(String[] args) {
        String name = "Shiv prakash tripathi";

        getRepeatedCharacters(name).stream()
                .forEach(characterIntegerEntry -> System.out.println("Key : " + characterIntegerEntry.getKey() + " , Value : " + characterIntegerEntry.getValue()));
    }

    private static Set<Map.Entry<Character, Integer>> getRepeatedCharacters(String name) {
        name = name.toLowerCase();

        HashMap<Character,Integer> characterMap = new HashMap<>();

        for(Character ch :  name.toCharArray()){
            characterMap.merge(ch,1,Integer::sum);
        }

        return characterMap.entrySet().stream()
                .filter(entry -> entry.getValue()>1)
                .collect(Collectors.toSet());
    }
}

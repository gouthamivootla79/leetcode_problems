package sample;

import java.util.LinkedHashMap;
import java.util.Map;

public class sample10 {
    public static void main(String[] args) {
        String word = "WorkingJavaIsFun";
        function(word);
    }

    private static void function(String word) {

        String lowerCaseWord = word.toLowerCase();
        Map<Character,Character> map = new LinkedHashMap<>();

        for(int i=0;i<word.length();i++){
            if(!map.containsKey((lowerCaseWord.charAt(i)))){
                map.put(lowerCaseWord.charAt(i),word.charAt(i));
            }
        }

        for(Map.Entry<Character,Character> entry: map.entrySet()){
            System.out.print(entry.getValue());
        }
    }
}

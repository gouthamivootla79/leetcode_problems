package sample;

import java.util.Arrays;

public class sample07 {
    public static void main(String[] args) {
        String[] states = {"Telangana","Andhra Pradesh","Arunachal Pradesh", "Uttar Pradesh", "Tamil Nadu"};

        //Bubble sort
        for(int i=0;i<states.length;i++){
            for(int j = i+1; j<states.length ;j++){
                System.out.println(states[i].compareTo(states[j]));
                if(states[i].compareTo(states[j]) > 0){
                    String temp = states[j];
                    states[j] = states[i];
                    states[i] = temp;
                }
            }
        }

        System.out.println(Arrays.toString(states));
    }
}

package sample;

public class Sample06 {
    public static void main(String[] args) {
        String str = "text123java456script1 4354 4344 5465werret45fdfd1";

//        Pattern p1 = Pattern.compile("[a-zA-Z]+");
//        Matcher m1 = p1.matcher(str);
//
//        while (m1.find()){
//            System.out.println(m1.group());
//        }

        String output = "";
//
//        for(Character charValue :  str.toCharArray()){
//
//            try{
//                Integer.parseInt(String.valueOf(charValue));
//                if(!output.equals("")){
//                    StringBuilder stringBuilder = new StringBuilder(output);
//                    System.out.print(stringBuilder.reverse());
//                }
//                System.out.print(charValue);
//                output = "";
//
//            }catch(Exception ex){
//                output = output + charValue;
//            }
//
//        }

        for (Character charValue : str.toCharArray()) {

            if (Character.isLetter(charValue)) {
                output = output + charValue;
            } else {
                if (!output.equals("")) {
                    StringBuilder stringBuilder = new StringBuilder(output);
                    System.out.print(stringBuilder.reverse());
                }
                System.out.print(charValue);
                output = "";
            }
        }
    }
}

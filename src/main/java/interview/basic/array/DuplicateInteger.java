package interview.basic.array;

public class DuplicateInteger {
    public static void main(String[] args) {
        int[] arr = {2, 3, 1, 4, 2, 5};

//        printDuplicateValue01(arr);
        printDuplicateValue02(arr);
    }

    // Here array is mutable, i.e can be modified
    private static void printDuplicateValue01(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            if (arr[Math.abs(arr[i])] > 0) {
                arr[Math.abs(arr[i])] = arr[Math.abs(arr[i])] * -1;
            } else {
                System.out.println("-" + Math.abs(arr[i]) + "-");
            }
        }
    }

    // Floyd Cycle Detection Algorithm
    // Array is immutable, cannot be modified
    private static void printDuplicateValue02(int[] arr) {

        int slow = arr[0];
        int fast = arr[0];

        // find the intersection point of slow and fast pointer
        do{
            slow = arr[slow];
            fast = arr[arr[fast]];
        }while(slow != fast);

        // mark fast pointer to 0th index and again find the intersection value
        fast = arr[0];
        while (fast!=slow){
            slow = arr[slow];
            fast = arr[fast];
        }

        System.out.println(fast);
    }
}

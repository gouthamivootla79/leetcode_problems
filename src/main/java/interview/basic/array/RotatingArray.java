package interview.basic.array;

import java.util.Arrays;

public class RotatingArray {
    public static void main(String[] args) {

        int[] arr = {1, 2, 3, 4, 5, 6};
        String rotate = "left";
        int d = 2;
        System.out.println(Arrays.toString(printRotatedArray(arr, "left", d)));
        System.out.println(Arrays.toString(printRotatedArray(arr, "right", d)));
    }

    private static int[] printRotatedArray(int[] arr, String rotate, int d) {

        int[] temp = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            if(rotate.equalsIgnoreCase("left"))
                temp[i] = arr[(i+d)% arr.length];
            else
                temp[i] = arr[(arr.length-d + i)% arr.length];
        }
        return temp;
    }
}

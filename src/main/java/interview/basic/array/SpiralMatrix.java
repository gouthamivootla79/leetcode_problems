package interview.basic.array;

public class SpiralMatrix {
    public static void main(String[] args) {
        int[][] nums = {{1,2,3},
                        {4,5,6},
                        {7,8,9},
                        {7,8,9}};
        int num = 1;
        int left = 0, right = nums[0].length - 1, top = 0, bottom = nums.length - 1;

        while (num <= nums.length * nums[0].length){

            for(int i=left;i<=right;i++){
                num++;
                System.out.print(nums[top][i]);
            }
            top++;

            for(int i=top;i<=bottom;i++){
                num++;
                System.out.print(nums[i][right]);
            }
            right--;

            for(int i=right;i>=left;i--){
                num++;
                System.out.print(nums[bottom][i]);
            }
            bottom--;

            for(int i=bottom;i>=top;i--){
                num++;
                System.out.print(nums[i][left]);
            }
            left++;
        }
    }
}

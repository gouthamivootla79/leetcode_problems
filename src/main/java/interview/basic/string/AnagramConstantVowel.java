package interview.basic.string;

import java.util.HashMap;

/**
 * str -> gado
 * print all anagram string , such that
 * - no 2 constant are adjacent
 * - no 2 vowel are adjacent
 */
public class AnagramConstantVowel {
    public static void main(String[] args) {
        String str = "aabyc";

        HashMap<Character, Integer> constantMap = new HashMap<>();
        HashMap<Character, Integer> vowelMap = new HashMap<>();

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'a' || str.charAt(i) == 'e' || str.charAt(i) == 'i' || str.charAt(i) == 'o' || str.charAt(i) == 'u') {
                vowelMap.merge(str.charAt(i), 1, Integer::sum);
            } else
                constantMap.merge(str.charAt(i), 1, Integer::sum);
        }
        if (checkEdgeCases(vowelMap, constantMap, str)) return;

        int value = fact(constantMap.size()) * fact(vowelMap.size());
        System.out.println("ans is : " + value);
    }

    private static boolean checkEdgeCases(HashMap<Character, Integer> vowelMap, HashMap<Character, Integer> constantMap, String str) {
        int vowelSum = vowelMap.entrySet().stream()
                .map(entrySet -> entrySet.getValue())
                .reduce(Integer::sum).get();

        int constantSum = constantMap.entrySet().stream()
                .map(entrySet -> entrySet.getValue())
                .reduce(Integer::sum).get();

        if (str.length() % 2 == 0) {
            if (constantSum != vowelSum) {
                System.out.println("No anagram");
                return true;
            }
        } else {
            if (constantSum != (vowelSum+1)) {
                System.out.println("No anagram");
                return true;
            }
        }
        return false;
    }

    private static int fact(int value) {
        if (value == 1)
            return 1;

        return fact(value - 1) * value;
    }
}

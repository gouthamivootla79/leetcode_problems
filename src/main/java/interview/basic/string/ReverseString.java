package interview.basic.string;

public class ReverseString {
    public static void main(String[] args) {
        String value = "Hello this is is Hello is Sp sP";
        String[] s = value.split(" ");

        for(int i=s.length-1;i>=0;i--){
            System.out.print(s[i] +" ");
        }
        System.out.println();

        String word = "Shiv";
        for (int i=0;i<word.length();i++){
            System.out.print(word.charAt(word.length()-1-i));
        }
        System.out.println();

        StringBuilder stringBuilder = new StringBuilder(word);
        System.out.println(stringBuilder.reverse());
    }
}

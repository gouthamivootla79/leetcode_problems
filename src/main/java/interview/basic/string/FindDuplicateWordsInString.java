package interview.basic.string;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FindDuplicateWordsInString {
    public static void main(String[] args) {
        String value = "Hello this is is Hello is Sp sP";

        //New Approach
        Map<String,Integer> strCount = new HashMap<>();
        for(String str : value.toLowerCase().split(" ")){
            strCount.merge(str,1,Integer::sum);
        }
        strCount.forEach((key, value1) -> System.out.println("Key : " + key + " , Value : " + value1));

        System.out.println("--------------");
        //Old Approach
        Map<String,Integer> strCountOld = new HashMap<>();
        for(String str : value.toLowerCase().split(" ")){
            if (strCountOld.containsKey(str)){
                strCountOld.put(str,strCountOld.get(str) +1);
            }else
                strCountOld.put(str, 1);
        }
        strCountOld.forEach((key, value1) -> System.out.println("Key : " + key + " , Value : " + value1));

        System.out.println("--------------");

        // Sort Map using Value in Asc Order
        List<Map.Entry<String, Integer>> listEntry = strCount.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toList());

        System.out.println(listEntry.get(listEntry.size() - 1).getKey());

    }
}

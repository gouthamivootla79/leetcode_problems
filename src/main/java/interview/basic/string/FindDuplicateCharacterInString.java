package interview.basic.string;

import java.util.HashMap;
import java.util.Map;

public class FindDuplicateCharacterInString {
    public static void main(String[] args) {
        String value = "Misissipim";
        Map<Character,Integer> charCount = new HashMap<>();
        for(Character ch : value.toLowerCase().toCharArray()){
            charCount.merge(ch,1,Integer::sum);
        }

        charCount.forEach((key, value1) -> System.out.println("Key : " + key + " , Value : " + value1));
    }
}

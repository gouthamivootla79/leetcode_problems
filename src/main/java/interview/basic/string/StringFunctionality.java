package interview.basic.string;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 *  \\b -> for boundary
 *  \\d or [0-9] -> would match only 1 digit
 *  \\d+ or [0-9]+-> would match whole number
 *  \\w -> would match letter, digit and underscore by 1 unit
 *  \\w+ -> would match letter, digit and underscore as a whole
 *  [^a-bA-B0-9]+ -> match all special characters
 */
public class StringFunctionality {
    public static void main(String[] args) {

        substringFunction();

        regularExpFunction();

    }

    private static void regularExpFunction() {
        String name02 = "Hello *** , my name is Shiv PT, gmail is abc@gmail.com , score is 453" +
                " url : https://codemine.in date:13/10/1994  mobile:8983744780";

        Pattern pattern01 = Pattern.compile("\\*{3}"); // for ***
        Pattern pattern02 = Pattern.compile("\\b[\\w._]+@[\\w._]+\\b"); // for email
        Pattern pattern03 = Pattern.compile("\\b\\d+\\b(?!/\\d{1,2}/\\d{4}\\b)"); // for int match
        Pattern pattern04 = Pattern.compile("\\bhttps://[\\w.]+\\b"); // for url
        Pattern pattern05 = Pattern.compile("\\b\\d{1,2}/\\d{1,2}/\\d{4}\\b"); // for date
        Pattern pattern06 = Pattern.compile("\\b\\d{10}\\b"); // for mobile number

        Matcher matcher;

        // Find and print ***
        matcher = pattern01.matcher(name02);
        while (matcher.find()) {
            System.out.println(matcher.group());
        }

        // Find and print email
        matcher = pattern02.matcher(name02);
        while (matcher.find()) {
            System.out.println(matcher.group());
        }

        // Find and print integers
        matcher = pattern03.matcher(name02);
        while (matcher.find()) {
            System.out.println(matcher.group());
        }

        // Find and print URLs
        matcher = pattern04.matcher(name02);
        while (matcher.find()) {
            System.out.println(matcher.group());
        }

        // Find and print dates
        matcher = pattern05.matcher(name02);
        while (matcher.find()) {
            System.out.println(matcher.group());
        }

        // Find and print mobile numbers
        matcher = pattern06.matcher(name02);
        while (matcher.find()) {
            System.out.println(matcher.group());
        }
    }

    private static void substringFunction() {
        String name = "Shiv Prakash tripathi";

        //substring get 'prakash' -> 0th indexing both, 5th index chosen and 12th position char is not picked/excluded
        System.out.println(name.substring(5,12));
        System.out.println(name.substring(5));
    }
}

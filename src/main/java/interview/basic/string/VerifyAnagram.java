package interview.basic.string;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * str1 -> listen
 * str2 -> silent
 * Here, both string length and chars are same, thus they are Anagram
 */
public class VerifyAnagram {
    public static void main(String[] args) {
        String str1= "listen";
        String str2= "silynt";

        System.out.println(checkAnagram(str1, str2));
    }

    private static boolean checkAnagram(String str1, String str2) {

        if(str1.length()!= str2.length())
            return false;

        HashMap<Character,Integer> map01 = new HashMap<>();
        HashMap<Character,Integer> map02 = new HashMap<>();

        for(int i=0;i<str2.length();i++){
            map01.merge(str1.charAt(i),1,Integer::sum);
            map02.merge(str2.charAt(i),1,Integer::sum);
        }

//        sample code, on how to sort map and return a lined hashmap
//        map01.entrySet().stream()
//                .sorted(Map.Entry.comparingByKey())
//                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue,(e1,e2) -> e1, LinkedHashMap::new));

        return map01.equals(map02);
    }
}

package interview.basic.sorting;

import java.util.Arrays;

//compare and swap
public class BubbleSort {
    public static void main(String[] args) {
        int[] arr = {5, 2, 1, 4, 7, 6};

        System.out.println(Arrays.toString(sort(arr)));
    }

    // TC-> O(N^2)
    private static int[] sort(int[] arr) {
        int flag = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length - i; j++) {
                if (arr[i] > arr[j]) {
                    // swap code
                    arr[i] = arr[i] + arr[j];
                    arr[j] = arr[i] - arr[j];
                    arr[i] = arr[i] - arr[j];
                    flag = 1;
                }
            }
            if (flag == 0)
                break;
        }

        return arr;
    }

}

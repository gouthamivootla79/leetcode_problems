package interview.basic.sorting;

import java.util.Arrays;

//Take an element and swap until it is in correct order
public class InsertionSort {
    public static void main(String[] args) {
        int[] arr = {5, 2, 1, 4, 7, 6};

        System.out.println(Arrays.toString(sort(arr)));
    }

    // TC-> O(N^2)
    private static int[] sort(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (arr[j] < arr[j - 1]) {
                    //swap the value
                    int temp = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = temp;
                }
            }
        }

        return arr;
    }
}

package interview.basic.sorting;

import java.util.Arrays;

//make a pivot, and move it to correct order
public class QuickSort {
    public static void main(String[] args) {
        int[] arr = {5, 2, 1, 4, 7, 6};

        sort(arr, 0, arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }

    private static void sort(int[] arr, int low, int high) {
        // if low< high , then only sorting can occur, otherwise , it is already sorted (low==high, 1 element)
        if (low < high) {

            //This will create pivot
            //Place it at correct place
            //Move all lesser ele in left side, and greater ele on right side of partition Index
            int partitionIndex = findPartition(arr, low, high);

            sort(arr, low, partitionIndex - 1);
            sort(arr, partitionIndex + 1, high);
        }
    }

    private static int findPartition(int[] arr, int low, int high) {
        int pivot = arr[low];

        int i = low;
        int j = high;

        while (i < j) {
            //increment i , if its value is less then pivot, stop when it gets greater element
            while (arr[i] <= pivot && i < high)
                i++;

            //decrement j , if its value is greater then pivot, stop when it gets smaller element
            while (arr[j] > pivot && j > low)
                j--;

            if (i < j) {
                // swap the arr[i] and arr[j] -> swap the greater and smaller element with each other
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }

        // swap the arr[low] and arr[j] i.e last element of jth index
        int temp = arr[low];
        arr[low] = arr[j];
        arr[j] = temp;

        return j;
    }
}

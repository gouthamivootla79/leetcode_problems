package interview.basic.sorting;

import java.util.Arrays;

//Select  min + swap
public class SelectionSort {
    public static void main(String[] args) {
        int[] arr = {5, 2, 1, 4, 7, 6};

        System.out.println(Arrays.toString(sort(arr)));
    }

    // TC-> O(N^2)
    private static int[] sort(int[] arr) {


        //finding the minimum value index
        for (int i = 0; i < arr.length; i++) {
            int min = i;
            for (int j = i; j < arr.length; j++) {
                if (arr[min] > arr[j])
                    min = j;
            }

            //swap the min value index
            int temp = arr[i];
            arr[i] = arr[min];
            arr[min] = temp;
        }
        return arr;
    }
}

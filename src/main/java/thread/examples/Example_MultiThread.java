package thread.examples;

public class Example_MultiThread {

    public static void main(String[] args) {
        Runnable runnable = () -> {
            String name = Thread.currentThread().getName();
            System.out.println(name + " Running...");
            System.out.println(name + " Finished...");
        };

        Thread thread01 = new Thread( runnable, "thread01");
        thread01.start();
        Thread thread02 = new Thread( runnable, "thread02");
        thread02.start();
        Thread thread03 = new Thread( runnable, "thread03");
        thread03.start();
    }
}

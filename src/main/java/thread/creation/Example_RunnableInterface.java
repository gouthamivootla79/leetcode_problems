package thread.creation;

public class Example_RunnableInterface {
    public static class MyRunnable implements Runnable{

        @Override
        public void run() {
            System.out.println("MyRunnable Running...");
            System.out.println("MyRunnable Finished...");
        }
    }

    public static void main(String[] args) {
        Thread thread = new Thread( new MyRunnable());
        thread.start();
    }
}

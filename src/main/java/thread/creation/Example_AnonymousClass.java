package thread.creation;

public class Example_AnonymousClass {

    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("MyRunnable Running...");
                System.out.println("MyRunnable Finished...");
            }
        };

        Thread thread = new Thread( runnable);
        thread.start();
    }
}

package thread.creation;

public class Example_LambdaExpression {

    public static void main(String[] args) {
        Runnable runnable = () -> {
            System.out.println("MyRunnable Running...");
            System.out.println("MyRunnable Finished...");
        };

        Thread thread = new Thread( runnable);
        thread.start();
    }
}

package thread.creation;

public class Example_ThreadClass {
    public static class MyThread extends Thread{

        @Override
        public void run() {
            super.run();
            System.out.println("MyThread Running...");
            System.out.println("MyThread Finished...");
        }
    }

    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        myThread.start();
    }
}
